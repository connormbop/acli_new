﻿namespace PorterHireDesktopApp
{
    partial class createQuote2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.CQ2Fuel = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cleanbox = new System.Windows.Forms.CheckBox();
            this.CQ2Cancel = new System.Windows.Forms.Button();
            this.CQ2Next = new System.Windows.Forms.Button();
            this.CQ2Back = new System.Windows.Forms.Button();
            this.ELabel = new System.Windows.Forms.Label();
            this.TLabel = new System.Windows.Forms.Label();
            this.MLabel = new System.Windows.Forms.Label();
            this.ModLabel = new System.Windows.Forms.Label();
            this.DayLabel = new System.Windows.Forms.Label();
            this.UserID = new System.Windows.Forms.Label();
            this.ClientID = new System.Windows.Forms.Label();
            this.CLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.locBox = new System.Windows.Forms.ComboBox();
            this.DLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create Quote";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(54, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(258, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Type  _______________________";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(54, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(263, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Make  _______________________";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(54, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Model  _______________________";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(54, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(301, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Equipment  _______________________";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(54, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(260, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Days  _______________________";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(54, 383);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Fuel:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(54, 440);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Transport:";
            // 
            // CQ2Fuel
            // 
            this.CQ2Fuel.Location = new System.Drawing.Point(113, 385);
            this.CQ2Fuel.Name = "CQ2Fuel";
            this.CQ2Fuel.Size = new System.Drawing.Size(180, 20);
            this.CQ2Fuel.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(54, 541);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 20);
            this.label9.TabIndex = 16;
            this.label9.Text = "Cleaning:";
            // 
            // cleanbox
            // 
            this.cleanbox.AutoSize = true;
            this.cleanbox.Location = new System.Drawing.Point(135, 546);
            this.cleanbox.Name = "cleanbox";
            this.cleanbox.Size = new System.Drawing.Size(15, 14);
            this.cleanbox.TabIndex = 17;
            this.cleanbox.UseVisualStyleBackColor = true;
            // 
            // CQ2Cancel
            // 
            this.CQ2Cancel.Location = new System.Drawing.Point(58, 624);
            this.CQ2Cancel.Name = "CQ2Cancel";
            this.CQ2Cancel.Size = new System.Drawing.Size(159, 42);
            this.CQ2Cancel.TabIndex = 18;
            this.CQ2Cancel.Text = "Cancel";
            this.CQ2Cancel.UseVisualStyleBackColor = true;
            this.CQ2Cancel.Click += new System.EventHandler(this.CQ2Cancel_Click);
            // 
            // CQ2Next
            // 
            this.CQ2Next.Location = new System.Drawing.Point(297, 624);
            this.CQ2Next.Name = "CQ2Next";
            this.CQ2Next.Size = new System.Drawing.Size(159, 42);
            this.CQ2Next.TabIndex = 19;
            this.CQ2Next.Text = "Next";
            this.CQ2Next.UseVisualStyleBackColor = true;
            this.CQ2Next.Click += new System.EventHandler(this.CQ2Next_Click);
            // 
            // CQ2Back
            // 
            this.CQ2Back.Location = new System.Drawing.Point(759, 755);
            this.CQ2Back.Name = "CQ2Back";
            this.CQ2Back.Size = new System.Drawing.Size(159, 42);
            this.CQ2Back.TabIndex = 20;
            this.CQ2Back.Text = "Back";
            this.CQ2Back.UseVisualStyleBackColor = true;
            this.CQ2Back.Click += new System.EventHandler(this.CQ2Back_Click);
            // 
            // ELabel
            // 
            this.ELabel.AutoSize = true;
            this.ELabel.Location = new System.Drawing.Point(176, 95);
            this.ELabel.Name = "ELabel";
            this.ELabel.Size = new System.Drawing.Size(41, 13);
            this.ELabel.TabIndex = 21;
            this.ELabel.Text = "label10";
            // 
            // TLabel
            // 
            this.TLabel.AutoSize = true;
            this.TLabel.Location = new System.Drawing.Point(176, 137);
            this.TLabel.Name = "TLabel";
            this.TLabel.Size = new System.Drawing.Size(41, 13);
            this.TLabel.TabIndex = 22;
            this.TLabel.Text = "label10";
            // 
            // MLabel
            // 
            this.MLabel.AutoSize = true;
            this.MLabel.Location = new System.Drawing.Point(176, 176);
            this.MLabel.Name = "MLabel";
            this.MLabel.Size = new System.Drawing.Size(41, 13);
            this.MLabel.TabIndex = 23;
            this.MLabel.Text = "label10";
            // 
            // ModLabel
            // 
            this.ModLabel.AutoSize = true;
            this.ModLabel.Location = new System.Drawing.Point(176, 218);
            this.ModLabel.Name = "ModLabel";
            this.ModLabel.Size = new System.Drawing.Size(41, 13);
            this.ModLabel.TabIndex = 24;
            this.ModLabel.Text = "label10";
            // 
            // DayLabel
            // 
            this.DayLabel.AutoSize = true;
            this.DayLabel.Location = new System.Drawing.Point(176, 272);
            this.DayLabel.Name = "DayLabel";
            this.DayLabel.Size = new System.Drawing.Size(41, 13);
            this.DayLabel.TabIndex = 25;
            this.DayLabel.Text = "label10";
            // 
            // UserID
            // 
            this.UserID.AutoSize = true;
            this.UserID.Location = new System.Drawing.Point(770, 20);
            this.UserID.Name = "UserID";
            this.UserID.Size = new System.Drawing.Size(41, 13);
            this.UserID.TabIndex = 26;
            this.UserID.Text = "label10";
            this.UserID.Visible = false;
            // 
            // ClientID
            // 
            this.ClientID.AutoSize = true;
            this.ClientID.Location = new System.Drawing.Point(894, 20);
            this.ClientID.Name = "ClientID";
            this.ClientID.Size = new System.Drawing.Size(41, 13);
            this.ClientID.TabIndex = 27;
            this.ClientID.Text = "label10";
            this.ClientID.Visible = false;
            // 
            // CLabel
            // 
            this.CLabel.AutoSize = true;
            this.CLabel.Location = new System.Drawing.Point(664, 20);
            this.CLabel.Name = "CLabel";
            this.CLabel.Size = new System.Drawing.Size(41, 13);
            this.CLabel.TabIndex = 28;
            this.CLabel.Text = "label10";
            this.CLabel.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label11.Location = new System.Drawing.Point(299, 385);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(265, 20);
            this.label11.TabIndex = 30;
            this.label11.Text = "Diesel: 1.99$per/L, Petrol: 2.5$per/L ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(321, 440);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 20);
            this.label10.TabIndex = 31;
            this.label10.Text = "$3.50 Per Kilometre";
            // 
            // locBox
            // 
            this.locBox.FormattingEnabled = true;
            this.locBox.Location = new System.Drawing.Point(142, 440);
            this.locBox.Name = "locBox";
            this.locBox.Size = new System.Drawing.Size(170, 21);
            this.locBox.TabIndex = 32;
            // 
            // DLabel
            // 
            this.DLabel.AutoSize = true;
            this.DLabel.Location = new System.Drawing.Point(582, 20);
            this.DLabel.Name = "DLabel";
            this.DLabel.Size = new System.Drawing.Size(41, 13);
            this.DLabel.TabIndex = 33;
            this.DLabel.Text = "label10";
            this.DLabel.Visible = false;
            // 
            // createQuote2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.DLabel);
            this.Controls.Add(this.locBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.CLabel);
            this.Controls.Add(this.ClientID);
            this.Controls.Add(this.UserID);
            this.Controls.Add(this.DayLabel);
            this.Controls.Add(this.ModLabel);
            this.Controls.Add(this.MLabel);
            this.Controls.Add(this.TLabel);
            this.Controls.Add(this.ELabel);
            this.Controls.Add(this.CQ2Back);
            this.Controls.Add(this.CQ2Next);
            this.Controls.Add(this.CQ2Cancel);
            this.Controls.Add(this.cleanbox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CQ2Fuel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "createQuote2";
            this.Text = "createQuote2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CQ2Fuel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cleanbox;
        private System.Windows.Forms.Button CQ2Cancel;
        private System.Windows.Forms.Button CQ2Next;
        private System.Windows.Forms.Button CQ2Back;
        private System.Windows.Forms.Label ELabel;
        private System.Windows.Forms.Label TLabel;
        private System.Windows.Forms.Label MLabel;
        private System.Windows.Forms.Label ModLabel;
        private System.Windows.Forms.Label DayLabel;
        private System.Windows.Forms.Label UserID;
        private System.Windows.Forms.Label ClientID;
        private System.Windows.Forms.Label CLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox locBox;
        private System.Windows.Forms.Label DLabel;
    }
}