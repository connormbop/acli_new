﻿namespace PorterHireDesktopApp
{
    partial class ManageEquipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStockName = new System.Windows.Forms.Label();
            this.lblStockDescription = new System.Windows.Forms.Label();
            this.StType = new System.Windows.Forms.TextBox();
            this.StDescr = new System.Windows.Forms.RichTextBox();
            this.lblManageStock = new System.Windows.Forms.Label();
            this.listStock = new System.Windows.Forms.ListBox();
            this.StockSave = new System.Windows.Forms.Button();
            this.StockDelete = new System.Windows.Forms.Button();
            this.lblStockPriceFuelType = new System.Windows.Forms.Label();
            this.lblStockLevel = new System.Windows.Forms.Label();
            this.lblStockPriceMonth = new System.Windows.Forms.Label();
            this.lblStockPriceWeek = new System.Windows.Forms.Label();
            this.lblEquipPriceDay = new System.Windows.Forms.Label();
            this.lblStockModel = new System.Windows.Forms.Label();
            this.lblStockMake = new System.Windows.Forms.Label();
            this.StPriceWeek = new System.Windows.Forms.TextBox();
            this.StPriceDay = new System.Windows.Forms.TextBox();
            this.StModel = new System.Windows.Forms.TextBox();
            this.StMake = new System.Windows.Forms.TextBox();
            this.StPriceMonth = new System.Windows.Forms.TextBox();
            this.StFuelType = new System.Windows.Forms.ComboBox();
            this.StockEdit = new System.Windows.Forms.Button();
            this.StockBack = new System.Windows.Forms.Button();
            this.txtStockMax = new System.Windows.Forms.Label();
            this.StLevel = new System.Windows.Forms.NumericUpDown();
            this.StockUpdate = new System.Windows.Forms.Button();
            this.hidID = new System.Windows.Forms.Label();
            this.SMFilter = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.StLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // lblStockName
            // 
            this.lblStockName.AutoSize = true;
            this.lblStockName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockName.Location = new System.Drawing.Point(56, 174);
            this.lblStockName.Name = "lblStockName";
            this.lblStockName.Size = new System.Drawing.Size(43, 20);
            this.lblStockName.TabIndex = 2;
            this.lblStockName.Text = "Type";
            // 
            // lblStockDescription
            // 
            this.lblStockDescription.AutoSize = true;
            this.lblStockDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockDescription.Location = new System.Drawing.Point(56, 55);
            this.lblStockDescription.Name = "lblStockDescription";
            this.lblStockDescription.Size = new System.Drawing.Size(89, 20);
            this.lblStockDescription.TabIndex = 7;
            this.lblStockDescription.Text = "Description";
            // 
            // StType
            // 
            this.StType.Location = new System.Drawing.Point(113, 174);
            this.StType.Name = "StType";
            this.StType.Size = new System.Drawing.Size(100, 20);
            this.StType.TabIndex = 9;
            // 
            // StDescr
            // 
            this.StDescr.Location = new System.Drawing.Point(171, 55);
            this.StDescr.Name = "StDescr";
            this.StDescr.Size = new System.Drawing.Size(248, 74);
            this.StDescr.TabIndex = 15;
            this.StDescr.Text = "";
            this.StDescr.TextChanged += new System.EventHandler(this.StockDescr_TextChanged);
            // 
            // lblManageStock
            // 
            this.lblManageStock.AutoSize = true;
            this.lblManageStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblManageStock.Location = new System.Drawing.Point(12, 9);
            this.lblManageStock.Name = "lblManageStock";
            this.lblManageStock.Size = new System.Drawing.Size(112, 20);
            this.lblManageStock.TabIndex = 16;
            this.lblManageStock.Text = "Manage Stock";
            // 
            // listStock
            // 
            this.listStock.FormattingEnabled = true;
            this.listStock.Location = new System.Drawing.Point(436, 128);
            this.listStock.Name = "listStock";
            this.listStock.Size = new System.Drawing.Size(536, 550);
            this.listStock.TabIndex = 17;
            // 
            // StockSave
            // 
            this.StockSave.Location = new System.Drawing.Point(58, 700);
            this.StockSave.Name = "StockSave";
            this.StockSave.Size = new System.Drawing.Size(75, 23);
            this.StockSave.TabIndex = 20;
            this.StockSave.Text = "Save";
            this.StockSave.UseVisualStyleBackColor = true;
            this.StockSave.Click += new System.EventHandler(this.StockSave_Click);
            // 
            // StockDelete
            // 
            this.StockDelete.Location = new System.Drawing.Point(267, 700);
            this.StockDelete.Name = "StockDelete";
            this.StockDelete.Size = new System.Drawing.Size(75, 23);
            this.StockDelete.TabIndex = 21;
            this.StockDelete.Text = "Delete";
            this.StockDelete.UseVisualStyleBackColor = true;
            this.StockDelete.Click += new System.EventHandler(this.StockDelete_Click);
            // 
            // lblStockPriceFuelType
            // 
            this.lblStockPriceFuelType.AutoSize = true;
            this.lblStockPriceFuelType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockPriceFuelType.Location = new System.Drawing.Point(54, 642);
            this.lblStockPriceFuelType.Name = "lblStockPriceFuelType";
            this.lblStockPriceFuelType.Size = new System.Drawing.Size(78, 20);
            this.lblStockPriceFuelType.TabIndex = 22;
            this.lblStockPriceFuelType.Text = "Fuel Type";
            // 
            // lblStockLevel
            // 
            this.lblStockLevel.AutoSize = true;
            this.lblStockLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockLevel.Location = new System.Drawing.Point(54, 602);
            this.lblStockLevel.Name = "lblStockLevel";
            this.lblStockLevel.Size = new System.Drawing.Size(91, 20);
            this.lblStockLevel.TabIndex = 23;
            this.lblStockLevel.Text = "Stock Level";
            // 
            // lblStockPriceMonth
            // 
            this.lblStockPriceMonth.AutoSize = true;
            this.lblStockPriceMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockPriceMonth.Location = new System.Drawing.Point(54, 538);
            this.lblStockPriceMonth.Name = "lblStockPriceMonth";
            this.lblStockPriceMonth.Size = new System.Drawing.Size(120, 20);
            this.lblStockPriceMonth.TabIndex = 24;
            this.lblStockPriceMonth.Text = "Price (Month)  $";
            // 
            // lblStockPriceWeek
            // 
            this.lblStockPriceWeek.AutoSize = true;
            this.lblStockPriceWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockPriceWeek.Location = new System.Drawing.Point(54, 471);
            this.lblStockPriceWeek.Name = "lblStockPriceWeek";
            this.lblStockPriceWeek.Size = new System.Drawing.Size(116, 20);
            this.lblStockPriceWeek.TabIndex = 25;
            this.lblStockPriceWeek.Text = "Price (Week)  $";
            // 
            // lblEquipPriceDay
            // 
            this.lblEquipPriceDay.AutoSize = true;
            this.lblEquipPriceDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblEquipPriceDay.Location = new System.Drawing.Point(54, 403);
            this.lblEquipPriceDay.Name = "lblEquipPriceDay";
            this.lblEquipPriceDay.Size = new System.Drawing.Size(103, 20);
            this.lblEquipPriceDay.TabIndex = 26;
            this.lblEquipPriceDay.Text = "Price (Day)  $";
            // 
            // lblStockModel
            // 
            this.lblStockModel.AutoSize = true;
            this.lblStockModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockModel.Location = new System.Drawing.Point(54, 332);
            this.lblStockModel.Name = "lblStockModel";
            this.lblStockModel.Size = new System.Drawing.Size(52, 20);
            this.lblStockModel.TabIndex = 27;
            this.lblStockModel.Text = "Model";
            // 
            // lblStockMake
            // 
            this.lblStockMake.AutoSize = true;
            this.lblStockMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockMake.Location = new System.Drawing.Point(54, 257);
            this.lblStockMake.Name = "lblStockMake";
            this.lblStockMake.Size = new System.Drawing.Size(48, 20);
            this.lblStockMake.TabIndex = 28;
            this.lblStockMake.Text = "Make";
            // 
            // StPriceWeek
            // 
            this.StPriceWeek.Location = new System.Drawing.Point(171, 471);
            this.StPriceWeek.Name = "StPriceWeek";
            this.StPriceWeek.Size = new System.Drawing.Size(100, 20);
            this.StPriceWeek.TabIndex = 29;
            // 
            // StPriceDay
            // 
            this.StPriceDay.Location = new System.Drawing.Point(155, 403);
            this.StPriceDay.Name = "StPriceDay";
            this.StPriceDay.Size = new System.Drawing.Size(100, 20);
            this.StPriceDay.TabIndex = 30;
            // 
            // StModel
            // 
            this.StModel.Location = new System.Drawing.Point(111, 332);
            this.StModel.Name = "StModel";
            this.StModel.Size = new System.Drawing.Size(100, 20);
            this.StModel.TabIndex = 31;
            // 
            // StMake
            // 
            this.StMake.Location = new System.Drawing.Point(111, 257);
            this.StMake.Name = "StMake";
            this.StMake.Size = new System.Drawing.Size(100, 20);
            this.StMake.TabIndex = 32;
            // 
            // StPriceMonth
            // 
            this.StPriceMonth.Location = new System.Drawing.Point(174, 538);
            this.StPriceMonth.Name = "StPriceMonth";
            this.StPriceMonth.Size = new System.Drawing.Size(100, 20);
            this.StPriceMonth.TabIndex = 34;
            // 
            // StFuelType
            // 
            this.StFuelType.FormattingEnabled = true;
            this.StFuelType.Location = new System.Drawing.Point(159, 640);
            this.StFuelType.Name = "StFuelType";
            this.StFuelType.Size = new System.Drawing.Size(121, 21);
            this.StFuelType.TabIndex = 35;
            // 
            // StockEdit
            // 
            this.StockEdit.Location = new System.Drawing.Point(436, 700);
            this.StockEdit.Name = "StockEdit";
            this.StockEdit.Size = new System.Drawing.Size(75, 23);
            this.StockEdit.TabIndex = 36;
            this.StockEdit.Text = "Edit";
            this.StockEdit.UseVisualStyleBackColor = true;
            this.StockEdit.Click += new System.EventHandler(this.StockEdit_Click);
            // 
            // StockBack
            // 
            this.StockBack.Location = new System.Drawing.Point(873, 774);
            this.StockBack.Name = "StockBack";
            this.StockBack.Size = new System.Drawing.Size(75, 23);
            this.StockBack.TabIndex = 37;
            this.StockBack.Text = "Back";
            this.StockBack.UseVisualStyleBackColor = true;
            this.StockBack.Click += new System.EventHandler(this.StockBack_Click);
            // 
            // txtStockMax
            // 
            this.txtStockMax.AutoSize = true;
            this.txtStockMax.Location = new System.Drawing.Point(264, 132);
            this.txtStockMax.Name = "txtStockMax";
            this.txtStockMax.Size = new System.Drawing.Size(155, 13);
            this.txtStockMax.TabIndex = 38;
            this.txtStockMax.Text = "Maximum 50 Characters: (0/50)";
            // 
            // StLevel
            // 
            this.StLevel.Location = new System.Drawing.Point(159, 602);
            this.StLevel.Name = "StLevel";
            this.StLevel.Size = new System.Drawing.Size(120, 20);
            this.StLevel.TabIndex = 39;
            // 
            // StockUpdate
            // 
            this.StockUpdate.Location = new System.Drawing.Point(159, 700);
            this.StockUpdate.Name = "StockUpdate";
            this.StockUpdate.Size = new System.Drawing.Size(75, 23);
            this.StockUpdate.TabIndex = 40;
            this.StockUpdate.Text = "Update";
            this.StockUpdate.UseVisualStyleBackColor = true;
            this.StockUpdate.Click += new System.EventHandler(this.StockUpdate_Click);
            // 
            // hidID
            // 
            this.hidID.AutoSize = true;
            this.hidID.Location = new System.Drawing.Point(171, 13);
            this.hidID.Name = "hidID";
            this.hidID.Size = new System.Drawing.Size(0, 13);
            this.hidID.TabIndex = 41;
            // 
            // SMFilter
            // 
            this.SMFilter.FormattingEnabled = true;
            this.SMFilter.Location = new System.Drawing.Point(436, 55);
            this.SMFilter.Name = "SMFilter";
            this.SMFilter.Size = new System.Drawing.Size(148, 21);
            this.SMFilter.TabIndex = 42;
            this.SMFilter.SelectedIndexChanged += new System.EventHandler(this.SMFilter_SelectedIndexChanged);
            // 
            // ManageEquipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.SMFilter);
            this.Controls.Add(this.hidID);
            this.Controls.Add(this.StockUpdate);
            this.Controls.Add(this.StLevel);
            this.Controls.Add(this.txtStockMax);
            this.Controls.Add(this.StockBack);
            this.Controls.Add(this.StockEdit);
            this.Controls.Add(this.StFuelType);
            this.Controls.Add(this.StPriceMonth);
            this.Controls.Add(this.StMake);
            this.Controls.Add(this.StModel);
            this.Controls.Add(this.StPriceDay);
            this.Controls.Add(this.StPriceWeek);
            this.Controls.Add(this.lblStockMake);
            this.Controls.Add(this.lblStockModel);
            this.Controls.Add(this.lblEquipPriceDay);
            this.Controls.Add(this.lblStockPriceWeek);
            this.Controls.Add(this.lblStockPriceMonth);
            this.Controls.Add(this.lblStockLevel);
            this.Controls.Add(this.lblStockPriceFuelType);
            this.Controls.Add(this.StockDelete);
            this.Controls.Add(this.StockSave);
            this.Controls.Add(this.listStock);
            this.Controls.Add(this.lblManageStock);
            this.Controls.Add(this.StDescr);
            this.Controls.Add(this.StType);
            this.Controls.Add(this.lblStockDescription);
            this.Controls.Add(this.lblStockName);
            this.Name = "ManageEquipment";
            this.Text = "ManageEquipment";
            ((System.ComponentModel.ISupportInitialize)(this.StLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStockName;
        private System.Windows.Forms.Label lblStockDescription;
        private System.Windows.Forms.TextBox StType;
        private System.Windows.Forms.RichTextBox StDescr;
        private System.Windows.Forms.Label lblManageStock;
        private System.Windows.Forms.ListBox listStock;
        private System.Windows.Forms.Button StockSave;
        private System.Windows.Forms.Button StockDelete;
        private System.Windows.Forms.Label lblStockPriceFuelType;
        private System.Windows.Forms.Label lblStockLevel;
        private System.Windows.Forms.Label lblStockPriceMonth;
        private System.Windows.Forms.Label lblStockPriceWeek;
        private System.Windows.Forms.Label lblEquipPriceDay;
        private System.Windows.Forms.Label lblStockModel;
        private System.Windows.Forms.Label lblStockMake;
        private System.Windows.Forms.TextBox StPriceWeek;
        private System.Windows.Forms.TextBox StPriceDay;
        private System.Windows.Forms.TextBox StModel;
        private System.Windows.Forms.TextBox StMake;
        private System.Windows.Forms.TextBox StPriceMonth;
        private System.Windows.Forms.ComboBox StFuelType;
        private System.Windows.Forms.Button StockEdit;
        private System.Windows.Forms.Button StockBack;
        private System.Windows.Forms.Label txtStockMax;
        private System.Windows.Forms.NumericUpDown StLevel;
        private System.Windows.Forms.Button StockUpdate;
        private System.Windows.Forms.Label hidID;
        private System.Windows.Forms.ComboBox SMFilter;
    }
}