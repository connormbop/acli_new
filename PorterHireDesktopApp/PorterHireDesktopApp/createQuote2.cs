﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;

namespace PorterHireDesktopApp
{
    public partial class createQuote2 : Form
    {
        //public bool clean;
        public createQuote2(string equipment, string type, string model, string make, int days, string ClientName, int staffID, float Cost)
        {
            InitializeComponent();
            SQL.selectQuery("SELECT DISTINCT LOCATION FROM DISTANCE D");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    locBox.Items.Add(SQL.read[0].ToString());
                }
            }
            CLabel.Text = Cost.ToString();
            UserID.Text = staffID.ToString();
            ClientID.Text = ClientName.ToString();
            ELabel.Text = equipment;
            TLabel.Text = type;
            MLabel.Text = model;
            ModLabel.Text = make;
            DayLabel.Text = days.ToString();
        }

        private void CQ2Back_Click(object sender, EventArgs e)
        {
            Hide();
            createQuote createQuote = new createQuote();
            createQuote.ShowDialog();
            this.Close();
        }

        public void CQ2Next_Click(object sender, EventArgs e)
        {
            float transport;
            SQL.selectQuery($"SELECT DISTINCT KM FROM DISTANCE WHERE LOCATION = '{locBox.SelectedItem}'");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    DLabel.Text = (SQL.read[0].ToString());
                }
            }

            bool clean = false;
            if (cleanbox.Checked)
            {
                clean = true;
            }
            else
            {
                clean = false;
            }
            
            if (CQ2Fuel.Text == "" || locBox.Text == "")
            {
                MessageBox.Show("Please fill in all fields before continuing");
            }
            else
            {
                transport = float.Parse(DLabel.Text);
                //float transport = float.Parse(CQ2Transport.Text);
                int sID = int.Parse(UserID.Text);
                int days = int.Parse(DayLabel.Text);
                float fuel = float.Parse(CQ2Fuel.Text);

                new createQuote3(ELabel.Text, TLabel.Text, ModLabel.Text, MLabel.Text, days, fuel, transport, locBox.SelectedItem.ToString(), clean, ClientID.Text, sID, CLabel.Text).Show();
                this.Hide();
            }
        }

        private void CQ2Cancel_Click(object sender, EventArgs e)
        {
            Hide();
            adminMenu adminMenu = new adminMenu();
            adminMenu.ShowDialog();
            this.Close();
        }
    }
}
