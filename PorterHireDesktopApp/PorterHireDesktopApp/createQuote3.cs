﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net.Mail;
using System.Net.Mime;

namespace PorterHireDesktopApp
{
    public partial class createQuote3 : Form
    {
        public string U_id = UserID.UnivUserID;
        public createQuote3(string equipment, string type, string model, string make, int days, float fuel, float transport,string location, bool clean, string cFN, int sID, string Cost)
        {
            InitializeComponent();
            SQL.selectQuery($"SELECT DISTINCT FuelType FROM PRODUCTS WHERE ProductModel = '{make}'");
            string FuelType = "";
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    FuelType = (SQL.read[0].ToString());
                }
            }
            float cleanCost = 60;
            double total = 0;
            locationName.Text = location;
            CQ3SN.Text = UserID.UnivStaffName;
            CQ3Price.Text = Cost;
            CQ3CN.Text = cFN.ToString();
            CQ3Equip.Text = equipment;
            CQ3Type.Text = type;
            CQ3Make.Text = model;
            CQ3Model.Text = make;
            CQ3Days.Text = days.ToString();
            CQ3Fuel.Text = fuel.ToString();
            CQ3Transport.Text = transport.ToString();
            total = double.Parse(CQ3Transport.Text);
            total = total * 3.5;
            CQ3Transport.Text = transport.ToString();
            if (FuelType == "Petrol")
            {
                total = (total + fuel) * 2.5;
            }
            else if (FuelType == "Diesel")
            {
                total = (total + fuel) * 1.99;
            }
            if (clean == true)
            {
                CQ3Cleaning.Text = cleanCost.ToString();
                total = total + 60;
            }
            total = total + double.Parse(Cost);
            total = total * 1.1;
            total = total * 1.15;
            CQ3TotalPrice.Text = total.ToString("F2");
        }

        private void CQ3Back_Click(object sender, EventArgs e)
        {
            int days = int.Parse(CQ3Days.Text);
            int sID = int.Parse(CQ3SN.Text);
            float Price = float.Parse(CQ3Price.Text);
            new createQuote2(CQ3Equip.Text, CQ3Type.Text, CQ3Make.Text, CQ3Model.Text, days, CQ3CN.Text, sID, Price).Show();
            this.Hide();
        }

        private void CQ3Cancel_Click(object sender, EventArgs e)
        {
            Hide();
            adminMenu adminMenu = new adminMenu();
            adminMenu.ShowDialog();
            this.Close();
        }

        private void createQuote3_Load(object sender, EventArgs e)
        {

        }

        private void CQ3CreateQuote_Click(object sender, EventArgs e)
        {
            SQL.selectQuery($"SELECT DISTINCT FuelType FROM PRODUCTS WHERE ProductModel = '{CQ3Model.Text}'");
            string FuelType = "";
            double total = double.Parse(CQ3TotalPrice.Text);
            double totalmgst = total / 0.15;
            DateTime now = DateTime.Now;
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    FuelType = (SQL.read[0].ToString());
                }
            }
            SQL.executeQuery($"INSERT INTO QUOTES (CID, CFN, CLN, CE, PID, PriceD, PriceW, PriceM, FType, FC, StaffID, UFN, FuelQuantityLitre, LOC, DIST, GST, INSURANCE, TotalMinusGST, TotalPlusGST, DateCreated,HiredDays, AcceptedORNotAccepted) values((SELECT DISTINCT CustomerID FROM CLIENTS WHERE C_FirstName = '{CQ3CN.Text}'),(SELECT DISTINCT C_FirstName FROM CLIENTS WHERE C_FirstName = '{CQ3CN.Text}'),(SELECT DISTINCT C_LastName FROM CLIENTS WHERE C_FirstName = '{CQ3CN.Text}'),(SELECT DISTINCT C_Email FROM CLIENTS WHERE C_FirstName = '{CQ3CN.Text}'),(SELECT DISTINCT ProductID from PRODUCTS where ProductModel = '{CQ3Model.Text}'),(SELECT DISTINCT ProductPriceDay FROM PRODUCTS WHERE ProductModel = '{CQ3Model.Text}'),(SELECT DISTINCT ProductPriceWeek FROM PRODUCTS WHERE ProductModel = '{CQ3Model.Text}'),(SELECT DISTINCT ProductPriceMonth FROM PRODUCTS WHERE ProductModel = '{CQ3Model.Text}'),(SELECT DISTINCT FuelType from FUEL where FuelType = '{FuelType}'),(SELECT DISTINCT FuelPrice from FUEL where FuelType = '{FuelType}'),(SELECT UserID FROM USERS where UserID = '{UserID.UnivUserID}'), (SELECT U_FirstName from USERS where U_Username = '{UserID.UnivStaffName}'), '{CQ3Fuel.Text}', '{locationName.Text}', '{CQ3Transport.Text}', 0.15, 0.10,'{totalmgst}', '{total}', '{now}', '{CQ3Days.Text}', 'Not Accepted')");
            MessageBox.Show("Quote Successfully Created");

            //****************************************ADD FROM HERE****************************************************************

            List<String> ClientID = new List<String>();
            List<String> ClientFName = new List<String>();
            List<String> ClientLName = new List<String>();
            List<String> ClientEmail = new List<String>();
            List<String> ClientCompany = new List<String>();

            SQL.selectQuery("SELECT * FROM CLIENTS WHERE C_FirstName = '" + CQ3CN.Text + "'");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ClientID.Add(SQL.read[0].ToString());
                    ClientFName.Add(SQL.read[1].ToString());
                    ClientLName.Add(SQL.read[2].ToString());
                    ClientEmail.Add(SQL.read[3].ToString());
                    ClientCompany.Add(SQL.read[4].ToString());
                }
            }

            //--------------------------------------PDF CREATION------------------------------------------------------------


            List<String> Reference = new List<String>();
            List<String> ProductID = new List<String>();
            List<String> Insurance = new List<String>();
            List<String> GST = new List<String>();
            List<String> TotalMinus = new List<String>();
            List<String> TotalPlus = new List<String>();
            List<String> DaysHired = new List<String>();
            List<String> Date = new List<String>();



            SQL.selectQuery("SELECT ReferenceID, PID, INSURANCE, GST, TotalMinusGST, TotalPlusGST, HiredDays, DateCreated  FROM QUOTES WHERE CID = '" + ClientID[0] + "'AND DateCreated= '" + DateTime.Now.ToString("dd-MM-yyyy") + "'");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Reference.Add(SQL.read[0].ToString());
                    ProductID.Add(SQL.read[1].ToString());
                    Insurance.Add(SQL.read[2].ToString());
                    GST.Add(SQL.read[3].ToString());
                    TotalMinus.Add(SQL.read[4].ToString());
                    TotalPlus.Add(SQL.read[5].ToString());
                    DaysHired.Add(SQL.read[6].ToString());
                    Date.Add(SQL.read[7].ToString());
                }
            }


            Document doc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("C:/acli_" + ClientID[0] + ".pdf", FileMode.Create));
            doc.Open();

            //LOGO
            iTextSharp.text.Image PorterLogo = iTextSharp.text.Image.GetInstance("porter-hire-logo.jpg");
            PorterLogo.SetAbsolutePosition(650, 500);
            PorterLogo.ScalePercent(50f);
            doc.Add(PorterLogo);

            //TEXT
            var Dochead = FontFactory.GetFont("Arial", 18.0f, 1, BaseColor.BLACK);
            var SubHead = FontFactory.GetFont("Arial", 12.0f, BaseColor.BLACK);
            var MoreInfo = FontFactory.GetFont("Arial", 12.0f, BaseColor.BLACK);


            Paragraph header = new Paragraph("PORTER HIRE QUOTE", Dochead);
            Paragraph subheading = new Paragraph("This is a detailed quote compiled by the Porter Hire Application", SubHead);
            Paragraph moreinfo = new Paragraph("Report compiled on: " + DateTime.Now.ToString("dd-MM-yyyy"), MoreInfo);
            Paragraph clientid = new Paragraph("Client ID: " + ClientID[0], MoreInfo);
            Paragraph clientname = new Paragraph("Client Name: " + ClientFName[0] + " " + ClientLName[0], MoreInfo);
            Paragraph clientemail = new Paragraph("Client Email: " + ClientEmail[0], MoreInfo);
            Paragraph clientcompname = new Paragraph("Client Company Name: " + ClientEmail[0], MoreInfo);



            PdfPTable QuoteTable = new PdfPTable(13);

            QuoteTable.DefaultCell.Padding = 3;
            QuoteTable.WidthPercentage = 100;
            QuoteTable.HorizontalAlignment = Element.ALIGN_LEFT;
            QuoteTable.DefaultCell.BorderWidth = 1;
            QuoteTable.SpacingBefore = 100f;

            var headFont = FontFactory.GetFont("Arial", 6.5f, 1, BaseColor.BLACK);
            var cellFont = FontFactory.GetFont("Arial", 6.5f, BaseColor.BLACK);

            iTextSharp.text.Font fontTinyItalic = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.ITALIC, BaseColor.GRAY);

            QuoteTable.AddCell("ReferenceID");
            QuoteTable.AddCell("ClientID");
            QuoteTable.AddCell("Client First Name");
            QuoteTable.AddCell("Client Last Name");
            QuoteTable.AddCell("Client Email");
            QuoteTable.AddCell("Client Company");
            QuoteTable.AddCell("ProductID");
            QuoteTable.AddCell("Insurance");
            QuoteTable.AddCell("GST COST");
            QuoteTable.AddCell("Total -GST");
            QuoteTable.AddCell("Total +GST");
            QuoteTable.AddCell("Days Hired");
            QuoteTable.AddCell("Date Created");

            QuoteTable.AddCell(Reference[0]);
            QuoteTable.AddCell(ClientID[0]);
            QuoteTable.AddCell(ClientFName[0]);
            QuoteTable.AddCell(ClientLName[0]);
            QuoteTable.AddCell(ClientEmail[0]);
            QuoteTable.AddCell(ClientCompany[0]);
            QuoteTable.AddCell(ProductID[0]);
            QuoteTable.AddCell(Insurance[0]);
            QuoteTable.AddCell(GST[0]);
            QuoteTable.AddCell(TotalMinus[0]);
            QuoteTable.AddCell(TotalPlus[0]);
            QuoteTable.AddCell(DaysHired[0]);
            QuoteTable.AddCell(Date[0]);




            QuoteTable.DefaultCell.Padding = 3;
            QuoteTable.WidthPercentage = 100;
            QuoteTable.HorizontalAlignment = Element.ALIGN_LEFT;
            QuoteTable.DefaultCell.BorderWidth = 1;
            QuoteTable.SpacingBefore = 100f;


            doc.Add(header);
            doc.Add(subheading);
            doc.Add(moreinfo);
            doc.Add(clientid);
            doc.Add(clientname);
            doc.Add(clientemail);
            doc.Add(clientcompname);
            doc.Add(QuoteTable);

            //CLOSE DOC
            doc.Close();
            ////--------------------------------------PDF CREATION------------------------------------------------------------


            ////--------------------------------------EMAIL CUSTOMER------------------------------------------------------------

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            Attachment att = new Attachment("porter-hire-logo.jpg", MediaTypeNames.Image.Jpeg);


            mail.From = new MailAddress("porterhirequoteapp@gmail.com");
            mail.To.Add(ClientEmail[0]);
            mail.Subject = "Porter Hire - Hello " + ClientFName[0];
            mail.Attachments.Add(new Attachment(@"C:/acli_" + ClientID[0] + ".pdf"));
            mail.Attachments.Add(att);
            mail.Body = "Attached below is the quote that was generated after your recent contact with our staff \r\n  Please contact us to accept the quote!";

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("porterhirequoteapp", "porterhire101");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            MessageBox.Show("Mail Sent");
            //--------------------------------------EMAIL CUSTOMER---------------------------------------------------------------
            ClientID.Clear();
            ClientFName.Clear();
            ClientLName.Clear();
            ClientEmail.Clear();
            ClientCompany.Clear();
        }
    }
}
