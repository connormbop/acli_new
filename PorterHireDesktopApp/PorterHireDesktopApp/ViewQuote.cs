﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;

using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace PorterHireDesktopApp
{
    public partial class ViewQuote : Form
    {
        public ViewQuote()
        {
            InitializeComponent();

            viewQuotes.View = View.Details;

            viewQuotes.Columns.Add("ID", 50, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Customer ID", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Customer First Name", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Customer Last Name", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Customer Email", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Product ID", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Product Price Day", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Product Price Week", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Product Price Month", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Fuel Type", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Fuel Cost", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Staff ID", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Staff First Name", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Fuel Quantity", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("GST", 50, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Insurance", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Total Minus GST", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Total Plus GST", 120, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Date Created", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Hired Days", 80, HorizontalAlignment.Left);
            viewQuotes.Columns.Add("Accepted OR Not Accepted", 120, HorizontalAlignment.Left);

            List<String> SearchFilters = new List<String>();

            SearchFilters.Add("ReferenceID");
            SearchFilters.Add("CID");
            SearchFilters.Add("CFN");
            SearchFilters.Add("CLN");

            searchQuoteFilerCombo.DataSource = SearchFilters;

        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            viewQuotes.Items.Clear();

            SQL.selectQuery("SELECT * FROM QUOTES WHERE " + searchQuoteFilerCombo.Text + " = '" + searchQuoteInput.Text + "'");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ListViewItem item = new ListViewItem(SQL.read[0].ToString());
                    item.SubItems.Add(SQL.read[1].ToString());
                    item.SubItems.Add(SQL.read[2].ToString());
                    item.SubItems.Add(SQL.read[3].ToString());
                    item.SubItems.Add(SQL.read[4].ToString());
                    item.SubItems.Add(SQL.read[5].ToString());
                    item.SubItems.Add(SQL.read[6].ToString());
                    item.SubItems.Add(SQL.read[7].ToString());
                    item.SubItems.Add(SQL.read[8].ToString());
                    item.SubItems.Add(SQL.read[9].ToString());
                    item.SubItems.Add(SQL.read[10].ToString());
                    item.SubItems.Add(SQL.read[11].ToString());
                    item.SubItems.Add(SQL.read[12].ToString());
                    item.SubItems.Add(SQL.read[13].ToString());
                    item.SubItems.Add(SQL.read[14].ToString());
                    item.SubItems.Add(SQL.read[15].ToString());
                    item.SubItems.Add(SQL.read[16].ToString());
                    item.SubItems.Add(SQL.read[17].ToString());
                    item.SubItems.Add(SQL.read[18].ToString());
                    item.SubItems.Add(SQL.read[19].ToString());
                    item.SubItems.Add(SQL.read[20].ToString());

                    viewQuotes.Items.Add(item);
                }
            }
        }


        private void showAll_Click(object sender, EventArgs e)
        {
            viewQuotes.Items.Clear();

            SQL.selectQuery("SELECT * FROM QUOTES");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ListViewItem item = new ListViewItem(SQL.read[0].ToString());
                    item.SubItems.Add(SQL.read[1].ToString());
                    item.SubItems.Add(SQL.read[2].ToString());
                    item.SubItems.Add(SQL.read[3].ToString());
                    item.SubItems.Add(SQL.read[4].ToString());
                    item.SubItems.Add(SQL.read[5].ToString());
                    item.SubItems.Add(SQL.read[6].ToString());
                    item.SubItems.Add(SQL.read[7].ToString());
                    item.SubItems.Add(SQL.read[8].ToString());
                    item.SubItems.Add(SQL.read[9].ToString());
                    item.SubItems.Add(SQL.read[10].ToString());
                    item.SubItems.Add(SQL.read[11].ToString());
                    item.SubItems.Add(SQL.read[12].ToString());
                    item.SubItems.Add(SQL.read[13].ToString());
                    item.SubItems.Add(SQL.read[14].ToString());
                    item.SubItems.Add(SQL.read[15].ToString());
                    item.SubItems.Add(SQL.read[16].ToString());
                    item.SubItems.Add(SQL.read[17].ToString());
                    item.SubItems.Add(SQL.read[18].ToString());
                    item.SubItems.Add(SQL.read[19].ToString());
                    item.SubItems.Add(SQL.read[20].ToString());

                    viewQuotes.Items.Add(item);
                }
            }
        }


        private void VQGenReport_Click(object sender, EventArgs e)
        {

            Document doc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("C:/acli_QuoteList.pdf", FileMode.Create));
            doc.Open();

            //LOGO
            iTextSharp.text.Image PorterLogo = iTextSharp.text.Image.GetInstance("porter-hire-logo.jpg");
            PorterLogo.SetAbsolutePosition(650, 500);
            PorterLogo.ScalePercent(50f);
            doc.Add(PorterLogo);

            //TEXT
            var Dochead = FontFactory.GetFont("Arial", 18.0f, 1, BaseColor.BLACK);
            var SubHead = FontFactory.GetFont("Arial", 12.0f, BaseColor.BLACK);
            var MoreInfo = FontFactory.GetFont("Arial", 12.0f, BaseColor.BLACK);

            Paragraph header = new Paragraph("PORTER HIRE QUOTE LIST", Dochead);
            Paragraph subheading = new Paragraph("This is a detailed quote report compiled by the Porter Hire Application", SubHead);
            Paragraph moreinfo = new Paragraph("Report compiled on: " + DateTime.Now.ToString("dd-MM-yyyy"), MoreInfo);

            doc.Add(header);
            doc.Add(subheading);
            doc.Add(moreinfo);

            //TABLE
            var headFont = FontFactory.GetFont("Arial", 6.5f, 1, BaseColor.BLACK);
            var cellFont = FontFactory.GetFont("Arial", 6.5f, BaseColor.BLACK);
            

            PdfPTable QuoteTable = new PdfPTable(viewQuotes.Columns.Count);

           
                
                QuoteTable.DefaultCell.Padding = 3;
                QuoteTable.WidthPercentage = 100;
                QuoteTable.HorizontalAlignment = Element.ALIGN_LEFT;
                QuoteTable.DefaultCell.BorderWidth = 1;
                QuoteTable.SpacingBefore = 100f;

            foreach (ColumnHeader column in viewQuotes.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.Text, headFont));
                    QuoteTable.AddCell(cell);
                }

                foreach (ListViewItem itemRow in viewQuotes.Items)
                {
                    int i = 0;
                    for (i = 0; i < itemRow.SubItems.Count; i++)
                    {
                    PdfPCell subcell = new PdfPCell(new Phrase(itemRow.SubItems[i].Text, cellFont));
                    QuoteTable.AddCell(subcell);                    
                }
                }

            doc.Add(QuoteTable);
            //CLOSE DOC
            doc.Close(); 

        }

        private void VQBack_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }

        private void VQGo_Click(object sender, EventArgs e)
        {

        }


    }
}
