using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;
using System.Net.Mail;


namespace PorterHireDesktopApp
{
    public partial class AddClient : Form
    {
        public AddClient()
        {
            InitializeComponent();

            label1.BackColor = System.Drawing.Color.Transparent;
            label2.BackColor = System.Drawing.Color.Transparent;
            label3.BackColor = System.Drawing.Color.Transparent;
            label4.BackColor = System.Drawing.Color.Transparent;
            label5.BackColor = System.Drawing.Color.Transparent;
            label6.BackColor = System.Drawing.Color.Transparent;
            label7.BackColor = System.Drawing.Color.Transparent;


            List<String> ClientsData = new List<String>();

            SQL.selectQuery("SELECT C_CompanyName FROM CLIENTS");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ClientsData.Add(SQL.read[0].ToString());
                }
            }


            ClientCombo.DataSource = ClientsData;
        }

        private void ClientCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //do something
        }

        private void addClientBtn_Click(object sender, EventArgs e)
        {
            string CustFN = "", CustLN = "", CustEm = "", CustPhone = "", CustCN = "";

            CustFN = CFname.Text.Trim();
            CustLN = CLname.Text.Trim();
            CustEm = CEmail.Text.Trim();
            CustPhone = CPhone.Text.Trim();
            CustCN = CCompanyName.Text.Trim();

            bool hasText = checkTextBoxes();
            if (!hasText)
            {
                MessageBox.Show("Please make sure all textboxes have text.");
                CFname.Focus();
                return;
            }
            else
            {
                SQL.executeQuery("INSERT into CLIENTS VALUES('" + CustFN + "','" + CustLN + "','" + CustEm + "', '" + CustCN + "', '" + CustPhone + "')");                
                MessageBox.Show( CustCN + " added to database!");
                
                //MessageBox.Show("Connection to database failed, Please try again!");


                //--------------------------------------EMAIL CUSTOMER------------------------------------------------------------
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("porterhirequoteapp@gmail.com");
                mail.To.Add(CEmail.Text);
                mail.Subject = "Porter Hire - Welcome";
                mail.Body = "Welcome to Porter Hire NZ \r\n You have been added to the database by a staff member \r\n Please contact us if this email has been sent to the wrong person \r\n ";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("porterhirequoteapp", "porterhire101");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("Mail Sent");
                clearfields();
                //--------------------------------------EMAIL CUSTOMER---------------------------------------------------------------
            }
        }

        private void updateClientBtn_Click(object sender, EventArgs e)
        {

            string CustFN = "", CustLN = "", CustEm = "", CustPhone = "", CustCN = "";

            CustFN = CFname.Text.Trim();
            CustLN = CLname.Text.Trim();
            CustEm = CEmail.Text.Trim();
            CustPhone = CPhone.Text.Trim();
            CustCN = CCompanyName.Text.Trim();

            DialogResult dialogResult = MessageBox.Show("Are you sure you wish to update the record for this client?(" + CCompanyName.Text + ")", "Warning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SQL.executeQuery("UPDATE CLIENTS SET C_FirstName='" + CustFN + "', C_LastName='" + CustLN + "', C_Email='" + CustEm + "', C_CompanyName= '" + CustCN + "', C_Phone= '" + CustPhone+ "' WHERE C_CompanyName='" + CCompanyName.Text + "'");
                MessageBox.Show("Client updated to database!");
                clearfields();
            }

            else if (dialogResult == DialogResult.No)
            {
                MessageBox.Show("No changes made");
            }
            MessageBox.Show("Connection to database failed, Please try again!");
        }

        private void deleteClientBtn_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you wish to delete this client? (" + CCompanyName.Text + ")", "Warning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SQL.executeQuery("DELETE FROM CLIENTS WHERE C_CompanyName= '" + CCompanyName.Text + "'");
                MessageBox.Show( CCompanyName.Text + " has been deleted from database!");
                clearfields();
            }
            else if (dialogResult == DialogResult.No)
            {
                MessageBox.Show("No changes made");
            }
        }

        private void selectClientBtn_Click(object sender, EventArgs e)
        {
            clearfields();
            string SelectedUSER = ClientCombo.Text;

            SQL.selectQuery("SELECT * FROM CLIENTS WHERE C_CompanyName = '" + SelectedUSER + "'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CFname.Text = SQL.read[1].ToString();
                    CLname.Text = SQL.read[2].ToString();
                    CEmail.Text = SQL.read[3].ToString();
                    CCompanyName.Text = SQL.read[4].ToString();
                    CPhone.Text = SQL.read[5].ToString();
                }
            }
        }

        private bool checkTextBoxes()
        {
            bool holdsData = true;
            //go through all of the controls
            foreach (Control c in this.Controls)
            {
                //if its a textbox, but doesnt matter if its middle textbox
                if (c is TextBox && (c != CPhone))
                {
                    //If it is not the case that it is empty
                    if ("".Equals((c as TextBox).Text.Trim()))
                    {
                        //set boolean to false because on textbox is empty
                        holdsData = false;
                    }
                }
            }
            //returns true or false based on if data is in all text boxs or not
            return holdsData;
        }

        private void clearfields()
        {
            CFname.Text = "";
            CLname.Text = "";
            CEmail.Text = "";
            CCompanyName.Text = "";
            CPhone.Text = "";
        }

        // ------------------------ AUTO FILL DETAILS (REMOVE) ----------------

        private void button1_Click(object sender, EventArgs e)
        {
            CFname.Text = "Test";
            CLname.Text = "Comp";
            CEmail.Text = "testcompany@gmail.com";
            CPhone.Text = "0800323232";
            CCompanyName.Text = "TestCompany";
        }

        // ------------------------ AUTO FILL DETAILS (REMOVE) ----------------

        private void button4_Click(object sender, EventArgs e)
        {
            Hide();
            userMenu userMenu = new userMenu();
            userMenu.ShowDialog();
            this.Close();
        }

        private void clear_Click(object sender, EventArgs e)
        {
            clearfields();
        }
    }
}
