﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;

namespace PorterHireDesktopApp
{

    public partial class StaffManagement : Form
    {
        
        public StaffManagement()
        {

            InitializeComponent();

            label1.BackColor = System.Drawing.Color.Transparent;
            label2.BackColor = System.Drawing.Color.Transparent;
            label3.BackColor = System.Drawing.Color.Transparent;
            label4.BackColor = System.Drawing.Color.Transparent;
            label5.BackColor = System.Drawing.Color.Transparent;
            label6.BackColor = System.Drawing.Color.Transparent;
            label7.BackColor = System.Drawing.Color.Transparent;
            label8.BackColor = System.Drawing.Color.Transparent;
            label9.BackColor = System.Drawing.Color.Transparent;

            List<String> USERStaff = new List<String>();
            List<String> ADMINStaff = new List<String>();
            List<String> StaffType = new List<String>();

            StaffType.Add("Admin");
            StaffType.Add("Standard User");

            SQL.selectQuery("SELECT U_Username FROM USERS U");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    USERStaff.Add(SQL.read[0].ToString());
                }
            }

            SQL.selectQuery("SELECT A_Username FROM ADMINS");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ADMINStaff.Add(SQL.read[0].ToString());
                }
            }

            adminCombo.DataSource = ADMINStaff;
            staffCombo.DataSource = USERStaff;
            staffTypeCombo.DataSource = StaffType;
            staffTypeCombo.SelectedIndex = 1;

        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            string fname = "", lname = "", email = "", phone = "", username = "", password = "";

            fname = fnameInput.Text.Trim();
            lname = lnameInput.Text.Trim();
            email = emailInput.Text.Trim();
            phone = phoneInput.Text.Trim();
            username = usernameInput.Text.Trim();
            password = passwordInput.Text.Trim();

            

            bool hasText = checkTextBoxes();
            if (!hasText)
            {
                MessageBox.Show("Please make sure all textboxes have text.");
                fnameInput.Focus();
                return;
            }

            else
            {
                if (staffTypeCombo.SelectedIndex == 1)
                {
                    SQL.executeQuery("INSERT into USERS VALUES('" + username + "','" + fname + "','" + lname + "', '" + email + "', '" + phone + "', '" + password + "')");
                    MessageBox.Show("Standard User Staff Memeber Added: "+ username);
                }

                else
                {
                    SQL.executeQuery("INSERT into ADMINS VALUES('" + username + "','" + fname + "','" + lname + "', '" + email + "', '" + phone + "', '" + password + "')");
                    MessageBox.Show("Admin Staff Memeber Added: " + username);
                }
            }

            
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            var x = 0;
            string fname = "", lname = "", email = "", phone = "", username = "", password = "";

            fname = fnameInput.Text.Trim();
            lname = lnameInput.Text.Trim();
            email = emailInput.Text.Trim();
            phone = phoneInput.Text.Trim();
            username = usernameInput.Text.Trim();
            password = passwordInput.Text.Trim();

            string SelectedUSERU = staffCombo.Text;
            string SelectedUSERA = adminCombo.Text;


            bool hasText = checkTextBoxes();
            if (!hasText)
            {
                MessageBox.Show("Please make sure all textboxes have text.");
                fnameInput.Focus();
                return;
            }

            else
            {

                DialogResult dialogResult = MessageBox.Show("Are you sure you wish to modify the record for the user: " + usernameInput.Text, "Warning", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SQL.selectQuery("SELECT U_Username,U_Password FROM USERS");
                    if (SQL.read.HasRows && x != 1)
                    {
                        SQL.executeQuery("UPDATE USERS SET U_Username='" + username + "', U_Firstname='" + fname + "', U_Lastname='" + lname + "', U_Email= '" + email + "', U_Phone= '" + phone + "', U_Password='" + password + "' WHERE U_Username='" + SelectedUSERU + "'");
                        adminCombo.Text = "";
                    }
                    

                    MessageBox.Show("Standard User Staff Memeber Updated: " + username);

                    clearfields();

                }

                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("No Changes Made");
                }
            }
            
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {

            

            DialogResult dialogResult = MessageBox.Show("Are you sure you wish to delete the user: " + usernameInput.Text, "Warning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SQL.executeQuery("DELETE FROM USERS WHERE U_Username= '" + usernameInput.Text + "'");
            }
            else if (dialogResult == DialogResult.No)
            {
                MessageBox.Show("No Changes Made");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Hide();
            adminMenu adminMenu = new adminMenu();
            adminMenu.ShowDialog();
            this.Close();
        }

        private bool checkTextBoxes()
        {
            bool holdsData = true;
            //go through all of the controls
            foreach (Control c in this.Controls)
            {
                //if its a textbox, but doesnt matter if its middle textbox
                if (c is TextBox && (c != phoneInput))
                {
                    //If it is not the case that it is empty
                    if ("".Equals((c as TextBox).Text.Trim()))
                    {
                        //set boolean to false because on textbox is empty
                        holdsData = false;
                    }
                }
            }
            //returns true or false based on if data is in all text boxs or not
            return holdsData;
        }


        private void selectUser_Click(object sender, EventArgs e)
        {
            clearfields();       
            string SelectedUSER = staffCombo.Text;

            SQL.selectQuery("SELECT * FROM USERS WHERE U_username = '" + SelectedUSER + "'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    usernameInput.Text = SQL.read[1].ToString();
                    fnameInput.Text = SQL.read[2].ToString();
                    lnameInput.Text = SQL.read[3].ToString();
                    emailInput.Text = SQL.read[4].ToString();
                    phoneInput.Text = SQL.read[5].ToString();
                    passwordInput.Text = SQL.read[6].ToString();
                    staffTypeCombo.Text = "";
                }
            }
        }


        private void selectAdmin_Click(object sender, EventArgs e)
        {
            clearfields();
            string SelectedUSER = adminCombo.Text;

            SQL.selectQuery("SELECT * FROM ADMINS WHERE A_username = '" + SelectedUSER + "'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    usernameInput.Text = SQL.read[1].ToString();
                    fnameInput.Text = SQL.read[2].ToString();
                    lnameInput.Text = SQL.read[3].ToString();
                    emailInput.Text = SQL.read[4].ToString();
                    phoneInput.Text = SQL.read[5].ToString();
                    passwordInput.Text = SQL.read[6].ToString();
                    staffTypeCombo.Text = "";
                }
            }
        }

        private void clearfields()
        {
            fnameInput.Text = "";
            lnameInput.Text = "";
            emailInput.Text = "";
            phoneInput.Text = "";
            usernameInput.Text = "";
            passwordInput.Text = "";
        }

        // ------------------------ AUTO FILL DETAILS (REMOVE) ----------------

        private void button1_Click(object sender, EventArgs e)
        {
            fnameInput.Text = "Connor";
            lnameInput.Text = "MacPherson";
            emailInput.Text = "connorMBOP@gmail.com";
            phoneInput.Text = "0220754198";
            usernameInput.Text = "connor123";
            passwordInput.Text = "connor123";
        }

        private void clear_Click(object sender, EventArgs e)
        {
            clearfields();
        }

        // ------------------------ AUTO FILL DETAILS (REMOVE) ----------------
    }
}
