﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace PorterHireDesktopApp
{
    public partial class ViewPricingAvailability : Form
    {
        public ViewPricingAvailability()
        {
            InitializeComponent();

            viewResults.View = View.Details;

            viewResults.Columns.Add("ID", 50, HorizontalAlignment.Left);
            viewResults.Columns.Add("Product Description", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Product Make", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Product Model", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Product Price Day", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Product Price Week", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Product Price Month", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Stock Level", 120, HorizontalAlignment.Left);
            viewResults.Columns.Add("Fuel Type", 100, HorizontalAlignment.Left);


            List<String> SearchFilters = new List<String>();

            SearchFilters.Add("ProductID");
            SearchFilters.Add("ProductDescription");
            SearchFilters.Add("ProductMake");
            SearchFilters.Add("ProductModel");

            searchFilterCombo.DataSource = SearchFilters;

        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            viewResults.Items.Clear();

            SQL.selectQuery("SELECT * FROM PRODUCTS WHERE " + searchFilterCombo.Text + " = '" + searchQueryInput.Text + "'");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ListViewItem item = new ListViewItem(SQL.read[0].ToString());
                    item.SubItems.Add(SQL.read[1].ToString());
                    item.SubItems.Add(SQL.read[2].ToString());
                    item.SubItems.Add(SQL.read[3].ToString());
                    item.SubItems.Add(SQL.read[4].ToString());
                    item.SubItems.Add(SQL.read[5].ToString());
                    item.SubItems.Add(SQL.read[6].ToString());
                    item.SubItems.Add(SQL.read[7].ToString());
                    item.SubItems.Add(SQL.read[8].ToString());
                    
                    viewResults.Items.Add(item);
                }
            }

            }

        private void showAll_Click(object sender, EventArgs e)
        {
            viewResults.Items.Clear();

            SQL.selectQuery("SELECT * FROM PRODUCTS");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    ListViewItem item = new ListViewItem(SQL.read[0].ToString());
                    item.SubItems.Add(SQL.read[1].ToString());
                    item.SubItems.Add(SQL.read[2].ToString());
                    item.SubItems.Add(SQL.read[3].ToString());
                    item.SubItems.Add(SQL.read[4].ToString());
                    item.SubItems.Add(SQL.read[5].ToString());
                    item.SubItems.Add(SQL.read[6].ToString());
                    item.SubItems.Add(SQL.read[7].ToString());
                    item.SubItems.Add(SQL.read[8].ToString());

                    viewResults.Items.Add(item);
                }
            }
        }

        private void VPAGenReport_Click(object sender, EventArgs e)
        {
            Document doc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("C:/acli_ProductList.pdf", FileMode.Create));
            doc.Open();

            //LOGO
            iTextSharp.text.Image PorterLogo = iTextSharp.text.Image.GetInstance("porter-hire-logo.jpg");
            PorterLogo.SetAbsolutePosition(650, 500);
            PorterLogo.ScalePercent(50f);
            doc.Add(PorterLogo);

            //TEXT
            var Dochead = FontFactory.GetFont("Arial", 18.0f, 1, BaseColor.BLACK);
            var SubHead = FontFactory.GetFont("Arial", 12.0f, BaseColor.BLACK);
            var MoreInfo = FontFactory.GetFont("Arial", 12.0f, BaseColor.BLACK);

            Paragraph header = new Paragraph("PORTER HIRE PRODUCT LIST", Dochead);
            Paragraph subheading = new Paragraph("This is a detailed product report compiled by the Porter Hire Application", SubHead);
            Paragraph moreinfo = new Paragraph("Report compiled on: " + DateTime.Now.ToString("dd-MM-yyyy"), MoreInfo);

            doc.Add(header);
            doc.Add(subheading);
            doc.Add(moreinfo);

            //TABLE
            var headFont = FontFactory.GetFont("Arial", 6.5f, 1, BaseColor.BLACK);
            var cellFont = FontFactory.GetFont("Arial", 6.5f, BaseColor.BLACK);


            PdfPTable QuoteTable = new PdfPTable(viewResults.Columns.Count);



            QuoteTable.DefaultCell.Padding = 3;
            QuoteTable.WidthPercentage = 100;
            QuoteTable.HorizontalAlignment = Element.ALIGN_LEFT;
            QuoteTable.DefaultCell.BorderWidth = 1;
            QuoteTable.SpacingBefore = 100f;

            foreach (ColumnHeader column in viewResults.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.Text, headFont));
                QuoteTable.AddCell(cell);
            }

            foreach (ListViewItem itemRow in viewResults.Items)
            {
                int i = 0;
                for (i = 0; i < itemRow.SubItems.Count; i++)
                {
                    PdfPCell subcell = new PdfPCell(new Phrase(itemRow.SubItems[i].Text, cellFont));
                    QuoteTable.AddCell(subcell);
                }
            }

            doc.Add(QuoteTable);
            //CLOSE DOC
            doc.Close();
        }

        private void VPABack_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }
    }
}
