﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PorterHireDesktopApp
{
    public partial class userMenu : Form
    {
        public userMenu()
        {
            InitializeComponent();
        }

        private void cQuoteBtn_Click(object sender, EventArgs e)
        {
            new createQuote().Show();
            this.Hide();
        }

        private void vPricingBtn_Click(object sender, EventArgs e)
        {
            new ViewPricingAvailability().Show();
            this.Hide();
        }

        private void addClientBtn_Click(object sender, EventArgs e)
        {
            new AddClient().Show();
            this.Hide();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            new index().Show();
            this.Hide();
        }
    }
}
