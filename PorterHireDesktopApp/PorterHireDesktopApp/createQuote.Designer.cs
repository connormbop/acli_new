﻿namespace PorterHireDesktopApp
{
    partial class createQuote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.CQEquip = new System.Windows.Forms.ComboBox();
            this.CQModel = new System.Windows.Forms.ComboBox();
            this.CQMake = new System.Windows.Forms.ComboBox();
            this.CQType = new System.Windows.Forms.ComboBox();
            this.CQDay = new System.Windows.Forms.Label();
            this.CQMonth = new System.Windows.Forms.Label();
            this.CQWeek = new System.Windows.Forms.Label();
            this.CQBack = new System.Windows.Forms.Button();
            this.CQStartDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CQEndDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.CQTotalPrice = new System.Windows.Forms.Label();
            this.CQCancel = new System.Windows.Forms.Button();
            this.CQNext = new System.Windows.Forms.Button();
            this.S_RBtn = new System.Windows.Forms.Button();
            this.CQCalculate = new System.Windows.Forms.Button();
            this.CQ1Client = new System.Windows.Forms.Label();
            this.CQ1ClientBox = new System.Windows.Forms.ComboBox();
            this.CQ1StaffID = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create Quote";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(54, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Equipment";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(54, 381);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Week  $_______________";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(54, 342);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(189, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Day  $_______________";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(54, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Model";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(54, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Make";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(54, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(54, 425);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(206, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Month  $_______________";
            // 
            // CQEquip
            // 
            this.CQEquip.FormattingEnabled = true;
            this.CQEquip.Location = new System.Drawing.Point(147, 95);
            this.CQEquip.Name = "CQEquip";
            this.CQEquip.Size = new System.Drawing.Size(187, 21);
            this.CQEquip.TabIndex = 8;
            this.CQEquip.SelectedIndexChanged += new System.EventHandler(this.CQEquip_SelectedIndexChanged);
            // 
            // CQModel
            // 
            this.CQModel.FormattingEnabled = true;
            this.CQModel.Location = new System.Drawing.Point(122, 223);
            this.CQModel.Name = "CQModel";
            this.CQModel.Size = new System.Drawing.Size(159, 21);
            this.CQModel.TabIndex = 9;
            // 
            // CQMake
            // 
            this.CQMake.FormattingEnabled = true;
            this.CQMake.Location = new System.Drawing.Point(122, 181);
            this.CQMake.Name = "CQMake";
            this.CQMake.Size = new System.Drawing.Size(172, 21);
            this.CQMake.TabIndex = 10;
            // 
            // CQType
            // 
            this.CQType.FormattingEnabled = true;
            this.CQType.Location = new System.Drawing.Point(122, 142);
            this.CQType.Name = "CQType";
            this.CQType.Size = new System.Drawing.Size(195, 21);
            this.CQType.TabIndex = 11;
            // 
            // CQDay
            // 
            this.CQDay.AutoSize = true;
            this.CQDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQDay.Location = new System.Drawing.Point(130, 333);
            this.CQDay.Name = "CQDay";
            this.CQDay.Size = new System.Drawing.Size(60, 20);
            this.CQDay.TabIndex = 12;
            this.CQDay.Text = "CQDay";
            // 
            // CQMonth
            // 
            this.CQMonth.AutoSize = true;
            this.CQMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQMonth.Location = new System.Drawing.Point(130, 414);
            this.CQMonth.Name = "CQMonth";
            this.CQMonth.Size = new System.Drawing.Size(77, 20);
            this.CQMonth.TabIndex = 13;
            this.CQMonth.Text = "CQMonth";
            // 
            // CQWeek
            // 
            this.CQWeek.AutoSize = true;
            this.CQWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQWeek.Location = new System.Drawing.Point(130, 372);
            this.CQWeek.Name = "CQWeek";
            this.CQWeek.Size = new System.Drawing.Size(73, 20);
            this.CQWeek.TabIndex = 14;
            this.CQWeek.Text = "CQWeek";
            // 
            // CQBack
            // 
            this.CQBack.Location = new System.Drawing.Point(812, 740);
            this.CQBack.Name = "CQBack";
            this.CQBack.Size = new System.Drawing.Size(159, 42);
            this.CQBack.TabIndex = 15;
            this.CQBack.Text = "Back";
            this.CQBack.UseVisualStyleBackColor = true;
            this.CQBack.Click += new System.EventHandler(this.CQBack_Click);
            // 
            // CQStartDate
            // 
            this.CQStartDate.Location = new System.Drawing.Point(160, 520);
            this.CQStartDate.Name = "CQStartDate";
            this.CQStartDate.Size = new System.Drawing.Size(200, 20);
            this.CQStartDate.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(54, 521);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Start Date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(54, 557);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "End Date";
            // 
            // CQEndDate
            // 
            this.CQEndDate.Location = new System.Drawing.Point(160, 556);
            this.CQEndDate.Name = "CQEndDate";
            this.CQEndDate.Size = new System.Drawing.Size(200, 20);
            this.CQEndDate.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label11.Location = new System.Drawing.Point(577, 598);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(235, 20);
            this.label11.TabIndex = 22;
            this.label11.Text = "Total Price  $_______________";
            // 
            // CQTotalPrice
            // 
            this.CQTotalPrice.AutoSize = true;
            this.CQTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQTotalPrice.Location = new System.Drawing.Point(682, 588);
            this.CQTotalPrice.Name = "CQTotalPrice";
            this.CQTotalPrice.Size = new System.Drawing.Size(102, 20);
            this.CQTotalPrice.TabIndex = 23;
            this.CQTotalPrice.Text = "CQTotalPrice";
            // 
            // CQCancel
            // 
            this.CQCancel.Location = new System.Drawing.Point(322, 656);
            this.CQCancel.Name = "CQCancel";
            this.CQCancel.Size = new System.Drawing.Size(159, 42);
            this.CQCancel.TabIndex = 24;
            this.CQCancel.Text = "Cancel";
            this.CQCancel.UseVisualStyleBackColor = true;
            this.CQCancel.Click += new System.EventHandler(this.CQCancel_Click);
            // 
            // CQNext
            // 
            this.CQNext.Location = new System.Drawing.Point(521, 656);
            this.CQNext.Name = "CQNext";
            this.CQNext.Size = new System.Drawing.Size(159, 42);
            this.CQNext.TabIndex = 25;
            this.CQNext.Text = "Next";
            this.CQNext.UseVisualStyleBackColor = true;
            this.CQNext.Click += new System.EventHandler(this.CQNext_Click);
            // 
            // S_RBtn
            // 
            this.S_RBtn.Location = new System.Drawing.Point(363, 95);
            this.S_RBtn.Name = "S_RBtn";
            this.S_RBtn.Size = new System.Drawing.Size(118, 21);
            this.S_RBtn.TabIndex = 26;
            this.S_RBtn.Text = "Search";
            this.S_RBtn.UseVisualStyleBackColor = true;
            this.S_RBtn.Click += new System.EventHandler(this.S_RBtn_Click);
            // 
            // CQCalculate
            // 
            this.CQCalculate.Location = new System.Drawing.Point(837, 592);
            this.CQCalculate.Name = "CQCalculate";
            this.CQCalculate.Size = new System.Drawing.Size(134, 26);
            this.CQCalculate.TabIndex = 27;
            this.CQCalculate.Text = "Calculate";
            this.CQCalculate.UseVisualStyleBackColor = true;
            this.CQCalculate.Click += new System.EventHandler(this.CQCalculate_Click);
            // 
            // CQ1Client
            // 
            this.CQ1Client.AutoSize = true;
            this.CQ1Client.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ1Client.Location = new System.Drawing.Point(54, 60);
            this.CQ1Client.Name = "CQ1Client";
            this.CQ1Client.Size = new System.Drawing.Size(53, 20);
            this.CQ1Client.TabIndex = 28;
            this.CQ1Client.Text = "Client:";
            // 
            // CQ1ClientBox
            // 
            this.CQ1ClientBox.FormattingEnabled = true;
            this.CQ1ClientBox.Location = new System.Drawing.Point(122, 59);
            this.CQ1ClientBox.Name = "CQ1ClientBox";
            this.CQ1ClientBox.Size = new System.Drawing.Size(187, 21);
            this.CQ1ClientBox.TabIndex = 29;
            // 
            // CQ1StaffID
            // 
            this.CQ1StaffID.FormattingEnabled = true;
            this.CQ1StaffID.Location = new System.Drawing.Point(395, 59);
            this.CQ1StaffID.Name = "CQ1StaffID";
            this.CQ1StaffID.Size = new System.Drawing.Size(187, 21);
            this.CQ1StaffID.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.Location = new System.Drawing.Point(327, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 20);
            this.label12.TabIndex = 30;
            this.label12.Text = "StaffID";
            // 
            // createQuote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.CQ1StaffID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CQ1ClientBox);
            this.Controls.Add(this.CQ1Client);
            this.Controls.Add(this.CQCalculate);
            this.Controls.Add(this.S_RBtn);
            this.Controls.Add(this.CQNext);
            this.Controls.Add(this.CQCancel);
            this.Controls.Add(this.CQTotalPrice);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.CQEndDate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CQStartDate);
            this.Controls.Add(this.CQBack);
            this.Controls.Add(this.CQWeek);
            this.Controls.Add(this.CQMonth);
            this.Controls.Add(this.CQDay);
            this.Controls.Add(this.CQType);
            this.Controls.Add(this.CQMake);
            this.Controls.Add(this.CQModel);
            this.Controls.Add(this.CQEquip);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "createQuote";
            this.Text = "createQuote";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CQEquip;
        private System.Windows.Forms.ComboBox CQModel;
        private System.Windows.Forms.ComboBox CQMake;
        private System.Windows.Forms.ComboBox CQType;
        private System.Windows.Forms.Label CQDay;
        private System.Windows.Forms.Label CQMonth;
        private System.Windows.Forms.Label CQWeek;
        private System.Windows.Forms.Button CQBack;
        private System.Windows.Forms.DateTimePicker CQStartDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker CQEndDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label CQTotalPrice;
        private System.Windows.Forms.Button CQCancel;
        private System.Windows.Forms.Button CQNext;
        private System.Windows.Forms.Button S_RBtn;
        private System.Windows.Forms.Button CQCalculate;
        private System.Windows.Forms.Label CQ1Client;
        private System.Windows.Forms.ComboBox CQ1ClientBox;
        private System.Windows.Forms.ComboBox CQ1StaffID;
        private System.Windows.Forms.Label label12;
    }
}