﻿namespace PorterHireDesktopApp
{
    partial class ViewPricingAvailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.searchFilterCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.searchQueryInput = new System.Windows.Forms.TextBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.viewResults = new System.Windows.Forms.ListView();
            this.showAll = new System.Windows.Forms.Button();
            this.VPABack = new System.Windows.Forms.Button();
            this.VPAGenReport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Check Pricing && Availability";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(500, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Please use the search filter and keyword section to browse equipment";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Search:";
            // 
            // searchFilterCombo
            // 
            this.searchFilterCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchFilterCombo.FormattingEnabled = true;
            this.searchFilterCombo.Location = new System.Drawing.Point(17, 155);
            this.searchFilterCombo.Name = "searchFilterCombo";
            this.searchFilterCombo.Size = new System.Drawing.Size(171, 28);
            this.searchFilterCombo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Search Filter:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(242, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Search Query:";
            // 
            // searchQueryInput
            // 
            this.searchQueryInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchQueryInput.Location = new System.Drawing.Point(246, 155);
            this.searchQueryInput.Name = "searchQueryInput";
            this.searchQueryInput.Size = new System.Drawing.Size(230, 26);
            this.searchQueryInput.TabIndex = 10;
            // 
            // searchBtn
            // 
            this.searchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBtn.Location = new System.Drawing.Point(491, 153);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(83, 28);
            this.searchBtn.TabIndex = 12;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // viewResults
            // 
            this.viewResults.Location = new System.Drawing.Point(17, 202);
            this.viewResults.Name = "viewResults";
            this.viewResults.Size = new System.Drawing.Size(955, 427);
            this.viewResults.TabIndex = 13;
            this.viewResults.UseCompatibleStateImageBehavior = false;
            // 
            // showAll
            // 
            this.showAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showAll.Location = new System.Drawing.Point(889, 155);
            this.showAll.Name = "showAll";
            this.showAll.Size = new System.Drawing.Size(83, 28);
            this.showAll.TabIndex = 14;
            this.showAll.Text = "Show All";
            this.showAll.UseVisualStyleBackColor = true;
            this.showAll.Click += new System.EventHandler(this.showAll_Click);
            // 
            // VPABack
            // 
            this.VPABack.Location = new System.Drawing.Point(805, 726);
            this.VPABack.Name = "VPABack";
            this.VPABack.Size = new System.Drawing.Size(116, 23);
            this.VPABack.TabIndex = 31;
            this.VPABack.Text = "Back";
            this.VPABack.UseVisualStyleBackColor = true;
            this.VPABack.Click += new System.EventHandler(this.VPABack_Click);
            // 
            // VPAGenReport
            // 
            this.VPAGenReport.Location = new System.Drawing.Point(593, 726);
            this.VPAGenReport.Name = "VPAGenReport";
            this.VPAGenReport.Size = new System.Drawing.Size(116, 23);
            this.VPAGenReport.TabIndex = 30;
            this.VPAGenReport.Text = "Generate Report";
            this.VPAGenReport.UseVisualStyleBackColor = true;
            this.VPAGenReport.Click += new System.EventHandler(this.VPAGenReport_Click);
            // 
            // ViewPricingAvailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.VPABack);
            this.Controls.Add(this.VPAGenReport);
            this.Controls.Add(this.showAll);
            this.Controls.Add(this.viewResults);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.searchQueryInput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.searchFilterCombo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ViewPricingAvailability";
            this.Text = "ViewPricingAvailability";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox searchFilterCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox searchQueryInput;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.ListView viewResults;
        private System.Windows.Forms.Button showAll;
        private System.Windows.Forms.Button VPABack;
        private System.Windows.Forms.Button VPAGenReport;
    }
}