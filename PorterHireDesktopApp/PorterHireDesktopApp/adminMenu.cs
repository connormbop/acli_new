﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PorterHireDesktopApp
{
    public partial class adminMenu : Form
    {
        public adminMenu()
        {
            InitializeComponent();
        }

        private void mStaff_Click(object sender, EventArgs e)
        {
             new StaffManagement().Show();
            this.Hide();
        }

        private void armEquipmentBtn_Click(object sender, EventArgs e)
        {
            new ManageEquipment().Show();
            this.Hide();
        }

        private void vQuotesBtn_Click(object sender, EventArgs e)
        {
            new ViewQuote().Show();
            this.Hide();
        }
        private void logoutBtn_Click(object sender, EventArgs e)
        {
            new index().Show();
            this.Hide();
        }
    }
}
