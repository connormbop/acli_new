﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;

namespace PorterHireDesktopApp
{
    public partial class ManageEquipment : Form
    {
        public string selID = null;
        public string selIndx = null;
        
        public ManageEquipment()
        {
            InitializeComponent();
            FuelTypePop();
            listStockPop();
            clearfields();
            listStockFilterPop();
            listStockSltdIndx();


        }
        

        void FuelTypePop()
        {

            StFuelType.Items.Add("Petrol");
            StFuelType.Items.Add("Diesel");
            StFuelType.SelectedIndex = 0;
        }

        void listStockPop()
        {
            List<String> SMlistStock = new List<String>();
            SQL.selectQuery("SELECT * FROM PRODUCTS");
            
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    
                    SMlistStock.Add(SQL.read[0].ToString() + ", " + //ID
                          SQL.read[1].ToString() + ", " + //Description
                          SQL.read[2].ToString() + ", " + //Type
                          SQL.read[3].ToString() + ", " + //Make
                          SQL.read[4].ToString() + ", " + //Model
                          double.Parse(SQL.read[5].ToString()).ToString("F2")  + ", " + //Day
                          double.Parse(SQL.read[6].ToString()).ToString("F2")  + ", " + //Week
                          double.Parse(SQL.read[7].ToString()).ToString("F2")  + ", " + //Month
                          SQL.read[8].ToString() + ", " + //Stock Lvl
                          SQL.read[9].ToString() + ", "); //Fuel
                    
                }
            }

            
            listStock.DataSource = SMlistStock;

            listStockSltdIndx();



        }

        void listStockSltdIndx()
        {
            if (selID != null)
            {
                listStock.SelectedIndex = Int32.Parse(selIndx);
            }
        }



        void clearfields()
        {
            StDescr.Text = "";
            StType.Text = "";
            StMake.Text = "";
            StModel.Text = "";
            StPriceDay.Text = "";
            StPriceWeek.Text = "";
            StPriceMonth.Text = "";
            StLevel.Value = 0;
            StFuelType.Text = "";
            hidID.Text = "";
        }

        private void StockDescr_TextChanged(object sender, EventArgs e)
        {
            StDescr.MaxLength = 50;
            string txtLength = StDescr.Text.Length.ToString();
            txtStockMax.Text = "Maximum 50 Characters: (" + txtLength + "/50)";
        }

        void listStockFilterPop()
        {
            
            

            List<String> SMDescripFilter = new List<String>();
            SMDescripFilter.Add("(no filter)");
            SQL.selectQuery("SELECT DISTINCT ProductDescription FROM PRODUCTS");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    
                    SMDescripFilter.Add(SQL.read[0].ToString());

                }
            }


            
            
            this.SMFilter.DataSource = SMDescripFilter;



        }



        private void StockSave_Click(object sender, EventArgs e)
        {

            if (StDescr.Text == "" || StType.Text == "" || StMake.Text == "" || StModel.Text == "" || StPriceDay.Text == "" || StPriceWeek.Text == "" || StPriceMonth.Text == "" ||  StFuelType.Text == "")
            {
                MessageBox.Show("You need to fill in all fields");
            }

            //else if (StLevel.Value == 0)
            //{
            //    MessageBox.Show("Stock Level cannot be 0");
            //}

            else
            {
                SQL.executeQuery("INSERT into PRODUCTS VALUES('" + StDescr.Text.Trim() + "', '"
                    + StType.Text.Trim() + "', '"
                    + StMake.Text.Trim() + "', '"
                    + StModel.Text.Trim() + "', '"
                    + StPriceDay.Text.Trim()  + "', '"
                    + StPriceWeek.Text.Trim()  + "', '"
                    + StPriceMonth.Text.Trim()  + "', '"
                    + StLevel.Value + "', '"
                    + StFuelType.Text.Trim() + "')"
                    );

                MessageBox.Show("New product has been added to the database successfully.");
                listStockPop();
                listStockFilterPop();
            }

            
            clearfields();
        }

        private void StockUpdate_Click(object sender, EventArgs e)
        {
            if (StDescr.Text == "" || StType.Text == "" || StMake.Text == "" || StModel.Text == "" || StPriceDay.Text == "" || StPriceWeek.Text == "" || StPriceMonth.Text == "" || StFuelType.Text == "")
            {
                MessageBox.Show("You need to fill in all fields");
            }

            else
            {
                SQL.executeQuery("UPDATE PRODUCTS SET ProductDescription='"
                + StDescr.Text.Trim() + "', " +
                "ProductType = '" + StType.Text.Trim() + "', " +
                "ProductMake = '" + StMake.Text.Trim() + "', " +
                "ProductModel = '" + StModel.Text.Trim() + "', " +
                "ProductPriceDay = '" + StPriceDay.Text.Trim() + "', " +
                "ProductPriceWeek = '" + StPriceWeek.Text.Trim() + "', " +
                "ProductPriceMonth = '" + StPriceMonth.Text.Trim() + "', " +
                "StockLevel = '" + StLevel.Value + "', " +
                "FuelType = '" + StFuelType.Text.Trim() + "' " +
                "WHERE ProductID='" + selID + "'"
                    );
                
                listStockPop();
                listStockFilterPop();
            }

            clearfields();

        }


        

        private void StockBack_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }



        private void StockEdit_Click(object sender, EventArgs e)
        {

            List<String> getID = new List<String>();
            SQL.selectQuery("SELECT ProductID FROM PRODUCTS");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    getID.Add(SQL.read[0].ToString());

                }
            }

            
            selIndx = listStock.SelectedIndex.ToString();
            selID = getID[listStock.SelectedIndex].ToString(); 

            ///////////////////////////
            List<String> SMDescription = new List<String>();
            List<String> SMType = new List<String>();
            List<String> SMMake = new List<String>();
            List<String> SMModel = new List<String>();
            List<String> SMPriceDay = new List<String>();
            List<String> SMPriceWeek = new List<String>();
            List<String> SMPriceMonth = new List<String>();
            List<String> SMStockLevel = new List<String>();
            List<String> SMFuelLevel = new List<String>();

            if (SMFilter.Text == "(no filter)")
            {
                SQL.selectQuery("SELECT * FROM PRODUCTS WHERE ProductID='" + selID + "'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        SMDescription.Add(SQL.read[1].ToString());

                        SMType.Add(SQL.read[2].ToString());

                        SMMake.Add(SQL.read[3].ToString());

                        SMModel.Add(SQL.read[4].ToString());

                        SMPriceDay.Add(double.Parse(SQL.read[5].ToString()).ToString("F2"));

                        SMPriceWeek.Add(double.Parse(SQL.read[6].ToString()).ToString("F2"));

                        SMPriceMonth.Add(double.Parse(SQL.read[7].ToString()).ToString("F2"));

                        SMStockLevel.Add(SQL.read[8].ToString());

                        SMFuelLevel.Add(SQL.read[9].ToString());


                    }
                }
            }

            else 
            {
                filterChoose();
                SQL.selectQuery("SELECT * FROM PRODUCTS WHERE ProductDescription = '" + SMFilter.Text + "' AND ProductID ='" + selID +"' ");
                
                
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        SMDescription.Add(SQL.read[1].ToString());

                        SMType.Add(SQL.read[2].ToString());

                        SMMake.Add(SQL.read[3].ToString());

                        SMModel.Add(SQL.read[4].ToString());

                        SMPriceDay.Add(double.Parse(SQL.read[5].ToString()).ToString("F2"));

                        SMPriceWeek.Add(double.Parse(SQL.read[6].ToString()).ToString("F2"));

                        SMPriceMonth.Add(double.Parse(SQL.read[7].ToString()).ToString("F2"));

                        SMStockLevel.Add(SQL.read[8].ToString());

                        SMFuelLevel.Add(SQL.read[9].ToString());

                        listStockSltdIndx();
                    }
                }
            }
            
 
            
            StDescr.Text = string.Join(",", SMDescription.ToArray());

            StType.Text = string.Join(",", SMType.ToArray());

            StMake.Text = string.Join(",", SMMake.ToArray());

            StModel.Text = string.Join(",", SMModel.ToArray());

            StPriceDay.Text = string.Join(",", SMPriceDay.ToArray());

            StPriceWeek.Text = string.Join(",", SMPriceWeek.ToArray());

            StPriceMonth.Text = string.Join(",", SMPriceMonth.ToArray());

            StLevel.Text = string.Join(",", SMStockLevel.ToArray());

            StFuelType.Text = string.Join(",", SMFuelLevel.ToArray());

            hidID.Text = selID;

        }//END OF StockEdit_Click

        private void StockDelete_Click(object sender, EventArgs e)
        {
            if (hidID.Text == "")
            {
                MessageBox.Show("You need to select a product, click 'Edit' and then you can delete.");
            }

            else
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to permanently delete this product from the database?", "Delete product from Database?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SQL.executeQuery("DELETE FROM PRODUCTS WHERE ProductID = '" + hidID.Text + "'");
                    listStockPop();
                    MessageBox.Show("Successfully deleted product: ID '" + hidID.Text + "', Description '" + StDescr.Text + "'");
                    clearfields();
                    listStockFilterPop();


                }
                else if (dialogResult == DialogResult.No)
                {
                    //nothing
                }

                
            }

            
        }

        private void SMFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterChoose();


        }

        void filterChoose()
        {
            if (SMFilter.Text == "(no filter)")
            {
                listStockPop();
            }

            else
            {
                List<String> getID = new List<String>();
                SQL.selectQuery("SELECT ProductID FROM PRODUCTS WHERE ProductDescription = '" + SMFilter.Text + "'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {

                        getID.Add(SQL.read[0].ToString());

                    }
                }

                selIndx = listStock.SelectedIndex.ToString();
                selID = getID[listStock.SelectedIndex].ToString();

                
                List<String> SMlistStock = new List<String>();

                SQL.selectQuery("SELECT * FROM PRODUCTS WHERE ProductDescription = '" + SMFilter.Text + "'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {

                        SMlistStock.Add(SQL.read[0].ToString() + ", " + //ID
                              SQL.read[1].ToString() + ", " + //Description
                              SQL.read[2].ToString() + ", " + //Type
                              SQL.read[3].ToString() + ", " + //Make
                              SQL.read[4].ToString() + ", " + //Model
                              double.Parse(SQL.read[5].ToString()).ToString("F2") + ", " + //Day
                              double.Parse(SQL.read[6].ToString()).ToString("F2") + ", " + //Week
                              double.Parse(SQL.read[7].ToString()).ToString("F2") + ", " + //Month
                              SQL.read[8].ToString() + ", " + //Stock Lvl
                              SQL.read[9].ToString() + ", "); //Fuel

                        getID.Add(SQL.read[0].ToString());
                    }
                }

                
                listStock.DataSource = SMlistStock;
            }
        }
    }
}
