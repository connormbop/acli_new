﻿namespace PorterHireDesktopApp
{
    partial class createQuote3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.CQ3CreateQuote = new System.Windows.Forms.Button();
            this.CQ3Cancel = new System.Windows.Forms.Button();
            this.CQ3Back = new System.Windows.Forms.Button();
            this.CQ3Equip = new System.Windows.Forms.Label();
            this.CQ3Price = new System.Windows.Forms.Label();
            this.CQ3Days = new System.Windows.Forms.Label();
            this.CQ3Model = new System.Windows.Forms.Label();
            this.CQ3Make = new System.Windows.Forms.Label();
            this.CQ3Type = new System.Windows.Forms.Label();
            this.CQ3Cleaning = new System.Windows.Forms.Label();
            this.CQ3Transport = new System.Windows.Forms.Label();
            this.CQ3Fuel = new System.Windows.Forms.Label();
            this.CQ3TotalPrice = new System.Windows.Forms.Label();
            this.CQ3SN = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.CQ3CN = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.locationName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(55, 411);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(264, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Fuel  $_______________________";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(54, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(258, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Type  _______________________";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(55, 351);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(268, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Price  $_______________________";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(54, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Model  _______________________";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(54, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Days  _______________________";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(54, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(301, 20);
            this.label2.TabIndex = 18;
            this.label2.Text = "Equipment  _______________________";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(54, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(263, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "Make  _______________________";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(55, 479);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(301, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Transport  $_______________________";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(54, 536);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(295, 20);
            this.label9.TabIndex = 20;
            this.label9.Text = "Cleaning  $_______________________";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(441, 536);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(307, 20);
            this.label10.TabIndex = 21;
            this.label10.Text = "Total Price  $_______________________";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(501, 573);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(194, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "(Includes 10% insurance and 15% GST)";
            // 
            // CQ3CreateQuote
            // 
            this.CQ3CreateQuote.Location = new System.Drawing.Point(525, 609);
            this.CQ3CreateQuote.Name = "CQ3CreateQuote";
            this.CQ3CreateQuote.Size = new System.Drawing.Size(159, 42);
            this.CQ3CreateQuote.TabIndex = 23;
            this.CQ3CreateQuote.Text = "Create Quote";
            this.CQ3CreateQuote.UseVisualStyleBackColor = true;
            this.CQ3CreateQuote.Click += new System.EventHandler(this.CQ3CreateQuote_Click);
            // 
            // CQ3Cancel
            // 
            this.CQ3Cancel.Location = new System.Drawing.Point(252, 609);
            this.CQ3Cancel.Name = "CQ3Cancel";
            this.CQ3Cancel.Size = new System.Drawing.Size(159, 42);
            this.CQ3Cancel.TabIndex = 24;
            this.CQ3Cancel.Text = "Cancel";
            this.CQ3Cancel.UseVisualStyleBackColor = true;
            this.CQ3Cancel.Click += new System.EventHandler(this.CQ3Cancel_Click);
            // 
            // CQ3Back
            // 
            this.CQ3Back.Location = new System.Drawing.Point(775, 727);
            this.CQ3Back.Name = "CQ3Back";
            this.CQ3Back.Size = new System.Drawing.Size(159, 42);
            this.CQ3Back.TabIndex = 25;
            this.CQ3Back.Text = "Back";
            this.CQ3Back.UseVisualStyleBackColor = true;
            this.CQ3Back.Click += new System.EventHandler(this.CQ3Back_Click);
            // 
            // CQ3Equip
            // 
            this.CQ3Equip.AutoSize = true;
            this.CQ3Equip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Equip.Location = new System.Drawing.Point(169, 86);
            this.CQ3Equip.Name = "CQ3Equip";
            this.CQ3Equip.Size = new System.Drawing.Size(0, 20);
            this.CQ3Equip.TabIndex = 26;
            // 
            // CQ3Price
            // 
            this.CQ3Price.AutoSize = true;
            this.CQ3Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Price.Location = new System.Drawing.Point(137, 342);
            this.CQ3Price.Name = "CQ3Price";
            this.CQ3Price.Size = new System.Drawing.Size(43, 20);
            this.CQ3Price.TabIndex = 27;
            this.CQ3Price.Text = "price";
            // 
            // CQ3Days
            // 
            this.CQ3Days.AutoSize = true;
            this.CQ3Days.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Days.Location = new System.Drawing.Point(137, 291);
            this.CQ3Days.Name = "CQ3Days";
            this.CQ3Days.Size = new System.Drawing.Size(0, 20);
            this.CQ3Days.TabIndex = 28;
            // 
            // CQ3Model
            // 
            this.CQ3Model.AutoSize = true;
            this.CQ3Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Model.Location = new System.Drawing.Point(137, 232);
            this.CQ3Model.Name = "CQ3Model";
            this.CQ3Model.Size = new System.Drawing.Size(0, 20);
            this.CQ3Model.TabIndex = 29;
            // 
            // CQ3Make
            // 
            this.CQ3Make.AutoSize = true;
            this.CQ3Make.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Make.Location = new System.Drawing.Point(137, 183);
            this.CQ3Make.Name = "CQ3Make";
            this.CQ3Make.Size = new System.Drawing.Size(0, 20);
            this.CQ3Make.TabIndex = 30;
            // 
            // CQ3Type
            // 
            this.CQ3Type.AutoSize = true;
            this.CQ3Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Type.Location = new System.Drawing.Point(137, 130);
            this.CQ3Type.Name = "CQ3Type";
            this.CQ3Type.Size = new System.Drawing.Size(0, 20);
            this.CQ3Type.TabIndex = 31;
            // 
            // CQ3Cleaning
            // 
            this.CQ3Cleaning.AutoSize = true;
            this.CQ3Cleaning.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Cleaning.Location = new System.Drawing.Point(169, 527);
            this.CQ3Cleaning.Name = "CQ3Cleaning";
            this.CQ3Cleaning.Size = new System.Drawing.Size(0, 20);
            this.CQ3Cleaning.TabIndex = 32;
            // 
            // CQ3Transport
            // 
            this.CQ3Transport.AutoSize = true;
            this.CQ3Transport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Transport.Location = new System.Drawing.Point(169, 470);
            this.CQ3Transport.Name = "CQ3Transport";
            this.CQ3Transport.Size = new System.Drawing.Size(0, 20);
            this.CQ3Transport.TabIndex = 33;
            // 
            // CQ3Fuel
            // 
            this.CQ3Fuel.AutoSize = true;
            this.CQ3Fuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3Fuel.Location = new System.Drawing.Point(137, 402);
            this.CQ3Fuel.Name = "CQ3Fuel";
            this.CQ3Fuel.Size = new System.Drawing.Size(0, 20);
            this.CQ3Fuel.TabIndex = 34;
            // 
            // CQ3TotalPrice
            // 
            this.CQ3TotalPrice.AutoSize = true;
            this.CQ3TotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3TotalPrice.Location = new System.Drawing.Point(578, 527);
            this.CQ3TotalPrice.Name = "CQ3TotalPrice";
            this.CQ3TotalPrice.Size = new System.Drawing.Size(60, 20);
            this.CQ3TotalPrice.TabIndex = 35;
            this.CQ3TotalPrice.Text = "label21";
            // 
            // CQ3SN
            // 
            this.CQ3SN.AutoSize = true;
            this.CQ3SN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3SN.Location = new System.Drawing.Point(170, 5);
            this.CQ3SN.Name = "CQ3SN";
            this.CQ3SN.Size = new System.Drawing.Size(0, 20);
            this.CQ3SN.TabIndex = 37;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label13.Location = new System.Drawing.Point(55, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(305, 20);
            this.label13.TabIndex = 36;
            this.label13.Text = "Staff Name  _______________________";
            // 
            // CQ3CN
            // 
            this.CQ3CN.AutoSize = true;
            this.CQ3CN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CQ3CN.Location = new System.Drawing.Point(170, 43);
            this.CQ3CN.Name = "CQ3CN";
            this.CQ3CN.Size = new System.Drawing.Size(0, 20);
            this.CQ3CN.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label15.Location = new System.Drawing.Point(55, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(310, 20);
            this.label15.TabIndex = 38;
            this.label15.Text = "Client Name  _______________________";
            // 
            // locationName
            // 
            this.locationName.AutoSize = true;
            this.locationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.locationName.Location = new System.Drawing.Point(910, 14);
            this.locationName.Name = "locationName";
            this.locationName.Size = new System.Drawing.Size(43, 20);
            this.locationName.TabIndex = 40;
            this.locationName.Text = "price";
            this.locationName.Visible = false;
            // 
            // createQuote3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 684);
            this.Controls.Add(this.locationName);
            this.Controls.Add(this.CQ3CN);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.CQ3SN);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.CQ3TotalPrice);
            this.Controls.Add(this.CQ3Fuel);
            this.Controls.Add(this.CQ3Transport);
            this.Controls.Add(this.CQ3Cleaning);
            this.Controls.Add(this.CQ3Type);
            this.Controls.Add(this.CQ3Make);
            this.Controls.Add(this.CQ3Model);
            this.Controls.Add(this.CQ3Days);
            this.Controls.Add(this.CQ3Price);
            this.Controls.Add(this.CQ3Equip);
            this.Controls.Add(this.CQ3Back);
            this.Controls.Add(this.CQ3Cancel);
            this.Controls.Add(this.CQ3CreateQuote);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Name = "createQuote3";
            this.Text = "createQuote3";
            this.Load += new System.EventHandler(this.createQuote3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button CQ3CreateQuote;
        private System.Windows.Forms.Button CQ3Cancel;
        private System.Windows.Forms.Button CQ3Back;
        private System.Windows.Forms.Label CQ3Equip;
        private System.Windows.Forms.Label CQ3Price;
        private System.Windows.Forms.Label CQ3Days;
        private System.Windows.Forms.Label CQ3Model;
        private System.Windows.Forms.Label CQ3Make;
        private System.Windows.Forms.Label CQ3Type;
        private System.Windows.Forms.Label CQ3Cleaning;
        private System.Windows.Forms.Label CQ3Transport;
        private System.Windows.Forms.Label CQ3Fuel;
        private System.Windows.Forms.Label CQ3TotalPrice;
        private System.Windows.Forms.Label CQ3SN;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label CQ3CN;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label locationName;
    }
}