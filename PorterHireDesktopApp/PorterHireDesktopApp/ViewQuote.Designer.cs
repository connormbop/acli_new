﻿namespace PorterHireDesktopApp
{
    partial class ViewQuote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VQBack = new System.Windows.Forms.Button();
            this.showAll = new System.Windows.Forms.Button();
            this.viewQuotes = new System.Windows.Forms.ListView();
            this.searchBtn = new System.Windows.Forms.Button();
            this.searchQuoteInput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.searchQuoteFilerCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.VQGenReport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // VQBack
            // 
            this.VQBack.Location = new System.Drawing.Point(856, 734);
            this.VQBack.Name = "VQBack";
            this.VQBack.Size = new System.Drawing.Size(116, 23);
            this.VQBack.TabIndex = 29;
            this.VQBack.Text = "Back";
            this.VQBack.UseVisualStyleBackColor = true;
            this.VQBack.Click += new System.EventHandler(this.VQBack_Click);
            // 
            // showAll
            // 
            this.showAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showAll.Location = new System.Drawing.Point(889, 155);
            this.showAll.Name = "showAll";
            this.showAll.Size = new System.Drawing.Size(83, 28);
            this.showAll.TabIndex = 39;
            this.showAll.Text = "Show All";
            this.showAll.UseVisualStyleBackColor = true;
            this.showAll.Click += new System.EventHandler(this.showAll_Click);
            // 
            // viewQuotes
            // 
            this.viewQuotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewQuotes.Location = new System.Drawing.Point(17, 202);
            this.viewQuotes.Name = "viewQuotes";
            this.viewQuotes.Size = new System.Drawing.Size(955, 427);
            this.viewQuotes.TabIndex = 38;
            this.viewQuotes.UseCompatibleStateImageBehavior = false;
            // 
            // searchBtn
            // 
            this.searchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBtn.Location = new System.Drawing.Point(491, 153);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(83, 28);
            this.searchBtn.TabIndex = 37;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // searchQuoteInput
            // 
            this.searchQuoteInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchQuoteInput.Location = new System.Drawing.Point(246, 155);
            this.searchQuoteInput.Name = "searchQuoteInput";
            this.searchQuoteInput.Size = new System.Drawing.Size(230, 26);
            this.searchQuoteInput.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(242, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 20);
            this.label5.TabIndex = 35;
            this.label5.Text = "Search Query:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 20);
            this.label4.TabIndex = 34;
            this.label4.Text = "Search Filter:";
            // 
            // searchQuoteFilerCombo
            // 
            this.searchQuoteFilerCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchQuoteFilerCombo.FormattingEnabled = true;
            this.searchQuoteFilerCombo.Location = new System.Drawing.Point(17, 155);
            this.searchQuoteFilerCombo.Name = "searchQuoteFilerCombo";
            this.searchQuoteFilerCombo.Size = new System.Drawing.Size(171, 28);
            this.searchQuoteFilerCombo.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 32;
            this.label3.Text = "Search:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(474, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "Please use the search filter and keyword section to browse quotes";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 26);
            this.label1.TabIndex = 30;
            this.label1.Text = "View Quotes";
            // 
            // VQGenReport
            // 
            this.VQGenReport.Location = new System.Drawing.Point(644, 734);
            this.VQGenReport.Name = "VQGenReport";
            this.VQGenReport.Size = new System.Drawing.Size(116, 23);
            this.VQGenReport.TabIndex = 27;
            this.VQGenReport.Text = "Generate Report";
            this.VQGenReport.UseVisualStyleBackColor = true;
            this.VQGenReport.Click += new System.EventHandler(this.VQGenReport_Click);
            // 
            // ViewQuote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.showAll);
            this.Controls.Add(this.viewQuotes);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.searchQuoteInput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.searchQuoteFilerCombo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.VQBack);
            this.Controls.Add(this.VQGenReport);
            this.Name = "ViewQuote";
            this.Text = "ViewQuote";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button VQBack;
        private System.Windows.Forms.Button showAll;
        private System.Windows.Forms.ListView viewQuotes;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox searchQuoteInput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox searchQuoteFilerCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button VQGenReport;
    }
}