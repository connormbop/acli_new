﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;

namespace PorterHireDesktopApp
{
    public partial class createQuote : Form
    {


        public int diff = 0;
        public createQuote()
        {
            InitializeComponent();

            SQL.selectQuery("SELECT DISTINCT ProductDescription FROM PRODUCTS P");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CQEquip.Items.Add(SQL.read[0].ToString());
                }
            }
            SQL.selectQuery("SELECT DISTINCT C_FirstName FROM CLIENTS C");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CQ1ClientBox.Items.Add(SQL.read[0].ToString());
                }
            }
            SQL.selectQuery("SELECT DISTINCT UserID FROM USERS U");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CQ1StaffID.Items.Add(SQL.read[0].ToString());
                }
            }




            //lbltest.Text = CQStartDate.Value.ToString("dd/MM/yyyy");
            //lbltest2.Text = CQEndDate.Value.ToString("dd/MM/yyyy");
        }

        public void CQNext_Click(object sender, EventArgs e)
        {
            if (CQEquip.Text == "" || CQMake.Text == "" || CQModel.Text == "" || CQType.Text == "")
            {
                MessageBox.Show("Please fill in all fields before continuing");
            }

            else
            {
                TimeSpan HireDiff = CQEndDate.Value - CQStartDate.Value;
                int days = HireDiff.Days + 1;
                int staffid = int.Parse(CQ1StaffID.SelectedItem.ToString());
                float Cost = float.Parse(CQTotalPrice.Text);
                new createQuote2(CQEquip.Text, CQType.Text, CQModel.Text, CQMake.Text, days, CQ1ClientBox.Text, staffid, Cost).Show();
                this.Close();
            }



            
        }

        private void CQCancel_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }

        private void CQEquip_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void S_RBtn_Click(object sender, EventArgs e)
        {
            CQType.Items.Clear();
            if (CQEquip.Text == "Excavators")
            {
                SQL.selectQuery("SELECT DISTINCT ProductType FROM PRODUCTS P where ProductDescription = 'Excavators'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQType.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductMake FROM PRODUCTS P where ProductDescription = 'Excavators'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQMake.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductModel FROM PRODUCTS P where ProductDescription = 'Excavators'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQModel.Items.Add(SQL.read[0].ToString());
                    }
                }
            }
            if (CQEquip.Text == "Tractors")
            {
                SQL.selectQuery("SELECT DISTINCT ProductType FROM PRODUCTS P where ProductDescription = 'Tractors'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQType.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductMake FROM PRODUCTS P where ProductDescription = 'Tractors'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQMake.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductModel FROM PRODUCTS P where ProductDescription = 'Tractors'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQModel.Items.Add(SQL.read[0].ToString());
                    }
                }
            }
            if (CQEquip.Text == "Rollers")
            {
                SQL.selectQuery("SELECT DISTINCT ProductType FROM PRODUCTS P where ProductDescription = 'Rollers'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQType.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductMake FROM PRODUCTS P where ProductDescription = 'Rollers'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQMake.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductModel FROM PRODUCTS P where ProductDescription = 'Rollers'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQModel.Items.Add(SQL.read[0].ToString());
                    }
                }
            }
            if (CQEquip.Text == "Bulldozers")
            {
                SQL.selectQuery("SELECT DISTINCT ProductType FROM PRODUCTS P where ProductDescription = 'Bulldozers'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQType.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductMake FROM PRODUCTS P where ProductDescription = 'Bulldozers'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQMake.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductModel FROM PRODUCTS P where ProductDescription = 'Bulldozers'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQModel.Items.Add(SQL.read[0].ToString());
                    }
                }
            }
            if (CQEquip.Text == "Loaders")
            {
                SQL.selectQuery("SELECT DISTINCT ProductType FROM PRODUCTS P where ProductDescription = 'Loaders'");

                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQType.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductMake FROM PRODUCTS P where ProductDescription = 'Loaders'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQMake.Items.Add(SQL.read[0].ToString());
                    }
                }
                SQL.selectQuery("SELECT DISTINCT ProductModel FROM PRODUCTS P where ProductDescription = 'Loaders'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        CQModel.Items.Add(SQL.read[0].ToString());
                    }
                }
            }
        }

        public void CQCalculate_Click(object sender, EventArgs e)
        {
            int dayValue =0;
            SQL.selectQuery($"SELECT DISTINCT ProductPriceDay FROM PRODUCTS P where ProductModel = '{CQModel.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CQDay.Text = (SQL.read[0].ToString());
                    dayValue = Convert.ToInt32(SQL.read[0]);
                }
            }
            SQL.selectQuery($"SELECT DISTINCT ProductPriceWeek FROM PRODUCTS P where ProductModel = '{CQModel.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CQWeek.Text = (SQL.read[0].ToString());
                }
            }
            SQL.selectQuery($"SELECT DISTINCT ProductPriceMonth FROM PRODUCTS P where ProductModel = '{CQModel.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CQMonth.Text = (SQL.read[0].ToString());
                }
            }

            TimeSpan HireDiff = CQEndDate.Value - CQStartDate.Value;
            int diff = HireDiff.Days +1;
            //string CQDayNumber = CQDay.Text;
            //var NumDays = int.Parse(dayValue);
            float total = 0;
            if (diff < 5)
            {
                total = diff * dayValue;
            }
            if (diff > 5 || diff < 29)
            {
                total = diff * dayValue;
            }
            if (diff > 29)
            {
                total = diff * dayValue;
            }

            CQTotalPrice.Text = total.ToString();
        }

        private void CQBack_Click(object sender, EventArgs e)
        {
            new adminMenu().Show(); ;
            this.Hide();
        }
    }
}
