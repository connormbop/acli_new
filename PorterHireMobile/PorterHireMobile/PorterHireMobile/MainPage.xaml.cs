﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace PorterHireMobile
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
           
        }

        async void LoginBtnClicked(object sender, EventArgs args)
        {
            Navigation.PushModalAsync(new ViewPricingAvailability());
            
        }
    }
}
