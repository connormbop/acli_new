﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PorterHireMobile
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewPricingAvailability : ContentPage
	{
		public ViewPricingAvailability ()
		{
			InitializeComponent ();

            List<String> SearchFilters = new List<String>();

            SearchFilters.Add("ProductID");
            SearchFilters.Add("ProductDescription");
            SearchFilters.Add("ProductMake");
            SearchFilters.Add("ProductModel");

            filterPicker.ItemsSource = SearchFilters;
        }
	}
}